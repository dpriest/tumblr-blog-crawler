var config = require("./config/index"); //Config file
var loader = require("./helpers/bootstrap"); //Let's bootstrap modules into memory

if (config.loader.app.bootstrap) {
    loader.bootstrap(config.loader.app.map);
}

var express = require('express');
var app = express(); // Our framework

var MongoClient = require('mongodb').MongoClient; // Mongo DB client

var cons = require('consolidate'); // Templating library
var cookieParser = require('cookie-parser'); //Cookie parser middleware
var bodyParser = require('body-parser'); //Body parser middleware
var logger = require('morgan'); //Logger middleware
var compression = require('compression'); //Compression middleware

var routes = require('./routes'); //Our routes
var daoFactory = require("./dao/daoFactory");
var taskManager = require("./helpers/taskManager"); //Task manager

exports.startApp = function(reload) {
    MongoClient.connect(config.mongodb_connection_line, function(err, db) {
        "use strict";
        if (err) {
            throw err;
        }

        // If mongodb is closed, close app
        db.on("close", function() {
            console.log("MongoDB has been closed");
            process.exit();
        });

        // Register our templating engine
        app.engine('html', cons.swig);
        app.set('view engine', 'html');
        app.set('views', __dirname + '/views');

        app.use(compression());

        // Express middleware to populate 'req.cookies' so we can access cookies
        app.use(cookieParser('optional secret string'));

        //Express middleware to log every query to app
        app.use(logger('short'));

        // Express middleware to populate 'req.body' so we can access POST variables
        app.use(bodyParser.json({limit: '100mb'}));
        app.use(bodyParser.urlencoded({extended: true, limit: '100mb'}));

        // Express middleware to return static files
        app.use(express.static(__dirname + config.static_dir, {maxAge: config.cache.time}));

        var manager = taskManager(config.taskManager, true);
        manager.on("start", function() {
            console.log("Starting work");
        }).on("finish", function() {
            console.log("Finished work");
        }).on("drain", function() {
            console.log("Drained!");
        });

        var factory = daoFactory(db, config);

        // Application routes
        routes(app, factory, manager, config);

        app.listen(config.listen_port, config.listen_ip);
        console.log('Express server listening on port ' + config.listen_port + " on address " + config.listen_ip);
    });

    // If slave process, reload after some time
    if (reload) {
        setTimeout(function() {
            console.log("Finished working");
            process.exit();
        }, config.live_time + Math.round(Math.random() * config.live_time_delta));
    }
};