/*
 * Common DAO tests
 */
var testDaoFactory = require("./testDaoFactory");

exports.getTests = function(testObject, daoMethod) {
    return {
        setUp: function(callback) {
            testDaoFactory.getDb(function(err, db) {
                if (err) {
                    throw err;
                }

                testObject.db = db;
                testObject.daoFactory = testDaoFactory.getDaoFactory(testObject.db);
                testObject.dao = testObject.daoFactory[daoMethod]();

                testObject.db.collection(testObject.dao.collection).remove({}, function(err) {
                    if (err) {
                        throw err;
                    }

                    callback();
                });
            });
        },
        insertSingleDoc: function(test) {
            test.expect(3);

            testObject.dao.insertSingleDoc({
                _id: "here is my key",
                payload: "here is my payload"
            }, function(err) {
                if (err) {
                    throw err;
                }

                testObject.db.collection(testObject.dao.collection).find({}, {}).toArray(function(err, docs) {
                    if (err) {
                        throw err;
                    }

                    test.ok(docs.length === 1, "Invalid amount of docs");
                    test.ok(docs[0]._id === "here is my key", "Invalid _id");
                    test.ok(docs[0].payload === "here is my payload", "Invalid payload");

                    test.done();
                });
            });
        },
        insertManyDocs: function(test) {
            var objects = {
                "id1": {payload: "payload for id 1", counter: 0},
                "id2": {payload: "payload for id 2", counter: 0},
                "id3": {payload: "payload for id 3", counter: 0},
                "id4": {payload: "payload for id 4", counter: 0},
                "id5": {payload: "payload for id 5", counter: 0}
            };

            var insertObjects = [];
            for (var id in objects) {
                insertObjects.push({_id: id, payload: objects[id].payload});
            }

            testObject.dao.insertManyDocs(insertObjects, function() {
                testObject.db.collection(testObject.dao.collection).find({}, {}).toArray(function(err, docs) {
                    if (err) {
                        throw err;
                    }

                    test.ok(docs.length === insertObjects.length, "Wrong amount of inserted docs");

                    docs.forEach(function(doc) {
                        if (!objects[doc._id]) {
                            throw new Error("Invalid object");
                        }

                        test.ok(doc.payload === objects[doc._id].payload, "Invalid object payload");

                        objects[doc._id].counter++;
                    });

                    for (var id in objects) {
                        if (objects[id].counter !== 1) {
                            throw new Error("Invalid amount of docs");
                        }
                    }

                    test.done();
                });
            });
        },
        updateSingleDoc: function(test) {
            test.expect(3);

            var oldPayload = "payload 1";
            var newPayload = "payload 2";
            var id = "id";

            testObject.db.collection(testObject.dao.collection).insert({_id: id, payload: oldPayload}, function(err) {
                if (err) {
                    throw err;
                }

                testObject.dao.updateSingleDoc({_id: id}, {$set: {payload: newPayload}}, function(err) {
                    if (err) {
                        throw err;
                    }

                    testObject.db.collection(testObject.dao.collection).find({}, {}).toArray(function(err, docs) {
                        if (err) {
                            throw err;
                        }

                        test.ok(docs.length === 1, "Invalid amount of docs");
                        test.ok(docs[0]._id === id, "Invalid _id");
                        test.ok(docs[0].payload === newPayload, "Invalid payload");

                        test.done();
                    });
                });
            });
        },
        getByQuery: function(test) {
            test.expect(7);

            var objects = {
                "id1": {sort: 1, payload: "payload for id 1"},
                "id2": {sort: 2, payload: "payload for id 2"},
                "id3": {sort: 3, payload: "payload for id 3"},
                "id4": {sort: 4, payload: "payload for id 4"},
                "id5": {sort: 5, payload: "payload for id 5"}
            };

            var insertObjects = [];
            for (var id in objects) {
                insertObjects.push({_id: id, payload: objects[id].payload, sort: objects[id].sort});
            }

            testObject.dao.insertManyDocs(insertObjects, function() {
                testObject.dao.getByQuery({sort: {$lt: 5}}, [["sort", -1]], 1, 2, function(err, docs, count) {
                    if (err) {
                        throw err;
                    }

                    test.ok(count === 4 && docs.length === 2, "Invalid amount of docs");
                    test.ok(docs[0]._id === "id3", "Invalid _id");
                    test.ok(docs[0].payload === objects[docs[0]._id].payload, "Invalid payload");
                    test.ok(docs[0].sort === objects[docs[0]._id].sort, "Invalid payload");
                    test.ok(docs[1]._id === "id2", "Invalid _id");
                    test.ok(docs[1].payload === objects[docs[1]._id].payload, "Invalid payload");
                    test.ok(docs[1].sort === objects[docs[1]._id].sort, "Invalid payload");

                    test.done();
                });
            });
        },
        remove: function(test) {
            test.expect(9);

            var objects = {
                "id1": {sort: 1, payload: "payload for id 1", counter: 0},
                "id2": {sort: 2, payload: "payload for id 2", counter: 0},
                "id3": {sort: 3, payload: "payload for id 3", counter: 0},
                "id4": {sort: 4, payload: "payload for id 4", counter: 0},
                "id5": {sort: 5, payload: "payload for id 5", counter: 0}
            };

            var insertObjects = [];
            for (var id in objects) {
                insertObjects.push({_id: id, payload: objects[id].payload, sort: objects[id].sort});
            }

            testObject.dao.insertManyDocs(insertObjects, function() {
                testObject.dao.remove({sort: {$mod: [2, 1]}}, function(err) {
                    if (err) {
                        throw err;
                    }

                    testObject.db.collection(testObject.dao.collection).find({}, {}).toArray(function(err, docs) {
                        if (err) {
                            throw err;
                        }

                        docs.forEach(function(doc) {
                            if (!objects[doc._id]) {
                                throw new Error("Invalid object");
                            }

                            test.ok(doc.payload === objects[doc._id].payload, "Invalid object payload");
                            test.ok(doc.sort === objects[doc._id].sort, "Invalid object sort");

                            objects[doc._id].counter++;
                        });

                        for (var id in objects) {
                            test.ok((objects[id].sort + 1) % 2 === objects[id].counter, "Invalid amount of objects");
                        }

                        test.done();
                    });
                });
            });
        },
        getByQueryIterative: function(test) {
            test.expect(7);

            var objects = {
                "id1": {sort: 1, payload: "payload for id 1"},
                "id2": {sort: 2, payload: "payload for id 2"},
                "id3": {sort: 3, payload: "payload for id 3"},
                "id4": {sort: 4, payload: "payload for id 4"},
                "id5": {sort: 5, payload: "payload for id 5"}
            };

            var insertObjects = [];
            for (var id in objects) {
                insertObjects.push({_id: id, payload: objects[id].payload, sort: objects[id].sort});
            }

            var cursor = 2;
            testObject.dao.insertManyDocs(insertObjects, function() {
                testObject.dao.getByQueryIterative({sort: {$lt: 5}}, [["sort", -1]], 1, 2, function(doc) {
                    test.ok(doc._id === insertObjects[cursor]._id, "Invalid _id");
                    test.ok(doc.payload === insertObjects[cursor].payload, "Invalid payload");
                    test.ok(doc.sort === insertObjects[cursor].sort, "Invalid sort");

                    cursor--;
                }, function(err) {
                    if (err) {
                        throw err;
                    }

                    test.ok(cursor === 0, "Invalid cursor value");

                    test.done();
                });
            });
        },
        tearDown: function(callback) {
            testObject.db.collection(testObject.dao.collection).remove({}, function(err) {
                if (err) {
                    throw err;
                }

                testObject.db.close(function(err) {
                    if (err) {
                        throw err;
                    }

                    callback();
                });
            });
        }
    };
};