/*
 * Generating test dao factory
 */

var connectLine = "mongodb://localhost:27017/tbc_test";
var MongoClient = require('mongodb').MongoClient;

exports.getDb = function(callback) {
    MongoClient.connect(connectLine, function(err, db) {
        if (err) {
            throw err;
        }

        callback(null, db);
    });
};

exports.getDaoFactory = function(db) {
    return require("./../../dao/daoFactory")(db, {});
};