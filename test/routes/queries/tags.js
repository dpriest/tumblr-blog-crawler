/**
 * Tag wrapper tests
 */

var testInsertRemove = function(func)
{
    return {
        emptyQuery: function(test) {
            test.expect(1);

            var tagQueryWrapper = require("./../../../routes/queries/tags")();
            var input = {};

            test.throws(function() {
                tagQueryWrapper[func](input);
            }, function(err) {
                return err.message === "Tag is missing";
            }, "Exception about missed _id should be displayed");

            test.done();
        },
        emptyId: function(test) {
            test.expect(1);

            var tagQueryWrapper = require("./../../../routes/queries/tags")();
            var input = {_id: ""};

            test.throws(function() {
                tagQueryWrapper[func](input);
            }, function(err) {
                return err.message === "Tag is missing";
            }, "Exception about empty _id should be displayed");

            test.done();
        },
        invalidId: function(test) {
            test.expect(3);

            var tagQueryWrapper = require("./../../../routes/queries/tags")();
            [
                {_id: ["234"]},
                {_id: 12},
                {_id: {"a": "boo"}}
            ].forEach(function(input) {
                test.throws(function() {
                    tagQueryWrapper[func](input);
                }, function(err) {
                    return err.message === "Invalid tag name value";
                }, "_id should be only string");
            });

            test.done();
        },
        validId: function(test) {
            test.expect(1);

            var tagQueryWrapper = require("./../../../routes/queries/tags")();
            var input = {_id: "kitties"};

            test.doesNotThrow(function() {
                tagQueryWrapper[func](input);
            }, "Valid tag query object");

            test.done();
        },
        finalCheck: function(test) {
            test.expect(1);

            var tagQueryWrapper = require("./../../../routes/queries/tags")();
            var input = {_id: "kitties"};

            var query = tagQueryWrapper[func](input);
            test.ok((query instanceof Object) && query._id === input._id, "Invalid tag query object");

            test.done();
        },
        wrongParameters: function(test) {
            test.expect(1);

            var tagQueryWrapper = require("./../../../routes/queries/tags")();
            var input = {_id: "kitties", order: 1, skip: 3};

            var query = tagQueryWrapper[func](input);
            test.ok((query instanceof Object) && query._id === input._id && Object.keys(query).length === 1, "Invalid tag query object");

            test.done();
        }
    };
};

exports.insert = testInsertRemove("insert");
exports.remove = testInsertRemove("remove");


exports.showTagList = {
    emptyQuery: function(test) {
        test.expect(4);

        var tagQueryWrapper = require("./../../../routes/queries/tags")();
        var input = {};
        var output = tagQueryWrapper.showTagList(input);

        test.ok(output instanceof Object && Object.keys(output).length === 3, "Wrong type or excess fields");
        test.ok(output.skip === 0, "Invalid skip");
        test.ok(output.order === 1, "Invalid order");
        test.ok(output.filter instanceof Object && Object.keys(output.filter).length === 0, "Wrong filter type or excess fields");

        test.done();
    },
    negativeSkip: function(test) {
        test.expect(1);

        var tagQueryWrapper = require("./../../../routes/queries/tags")();
        var input = {skip: "-10"};

        test.throws(function() {
            tagQueryWrapper.showTagList(input);
        }, function(err) {
            return err.message === "Page value must be integer";
        }, "Page number shouldn't be less than 0");

        test.done();
    },
    nonNumberSkip: function(test) {
        test.expect(1);

        var tagQueryWrapper = require("./../../../routes/queries/tags")();
        var input = {skip: ["11", 33]};

        test.throws(function() {
            tagQueryWrapper.showTagList(input);
        }, function(err) {
            return err.message === "Page value must be integer";
        }, "Page number should be integer");

        test.done();
    },
    validSkip: function(test) {
        test.expect(4);

        var tagQueryWrapper = require("./../../../routes/queries/tags")();
        var input = {skip: "25"};
        var output = tagQueryWrapper.showTagList(input);

        test.ok(output instanceof Object && Object.keys(output).length === 3, "Wrong type or excess fields");
        test.ok(output.skip === 25, "Invalid skip");
        test.ok(output.order === 1, "Invalid order");
        test.ok(output.filter instanceof Object && Object.keys(output.filter).length === 0, "Wrong filter type or excess fields");

        test.done();
    },
    invalidOrder: function(test) {
        test.expect(1);

        var tagQueryWrapper = require("./../../../routes/queries/tags")();
        var input = {order: "-10"};

        test.throws(function() {
            tagQueryWrapper.showTagList(input);
        }, function(err) {
            return err.message === "Order must be integer";
        }, "Order should be 1 or -1");

        test.done();
    },
    nonNumberOrder: function(test) {
        test.expect(1);

        var tagQueryWrapper = require("./../../../routes/queries/tags")();
        var input = {order: ["11", 33]};

        test.throws(function() {
            tagQueryWrapper.showTagList(input);
        }, function(err) {
            return err.message === "Order must be integer";
        }, "Order should be 1 or -1");

        test.done();
    },
    validOrder: function(test) {
        test.expect(4);

        var tagQueryWrapper = require("./../../../routes/queries/tags")();
        var input = {order: "-1"};
        var output = tagQueryWrapper.showTagList(input);

        test.ok(output instanceof Object && Object.keys(output).length === 3, "Wrong type or excess fields");
        test.ok(output.skip === 0, "Invalid skip");
        test.ok(output.order === -1, "Invalid order");
        test.ok(output.filter instanceof Object && Object.keys(output.filter).length === 0, "Wrong filter type or excess fields");

        test.done();
    },
    invalidId: function(test) {
        test.expect(3);

        var tagQueryWrapper = require("./../../../routes/queries/tags")();
        [
            {_id: {a: ["foo"], b: "bar", c: 123}},
            {_id: 222},
            {_id: ["a", "b", "c"]}
        ].forEach(function(input) {
            test.throws(function() {
                tagQueryWrapper.showTagList(input);
            }, function(err) {
                return err.message === "Invalid tag name value";
            }, "Tag name should be only string");
        });
        test.done();
    },
    validId: function(test) {
        test.expect(6);

        var tagQueryWrapper = require("./../../../routes/queries/tags")();
        var input = {_id: "lulzquery"};
        var output = tagQueryWrapper.showTagList(input);

        test.ok(output instanceof Object && Object.keys(output).length === 3, "Wrong type or excess fields");
        test.ok(output.skip === 0, "Invalid skip");
        test.ok(output.order === 1, "Invalid order");
        test.ok(output.filter instanceof Object && Object.keys(output.filter).length === 1, "Wrong filter type or excess fields");
        test.ok(output.filter._id instanceof Object && Object.keys(output.filter._id).length === 2, "Wrong id type or excess fields");
        test.ok(output.filter._id["$regex"] === "lulzquery" && output.filter._id["$options"] === "i", "Wrong id type or excess fields");

        test.done();
    },
    allIn: function(test) {
        test.expect(6);

        var tagQueryWrapper = require("./../../../routes/queries/tags")();
        var input = {_id: "foobar", skip: "4", order: "-1"};
        var output = tagQueryWrapper.showTagList(input);

        test.ok(output instanceof Object && Object.keys(output).length === 3, "Wrong type or excess fields");
        test.ok(output.skip === 4, "Invalid skip");
        test.ok(output.order === -1, "Invalid order");
        test.ok(output.filter instanceof Object && Object.keys(output.filter).length === 1, "Wrong filter type or excess fields");
        test.ok(output.filter._id instanceof Object && Object.keys(output.filter._id).length === 2, "Wrong id type or excess fields");
        test.ok(output.filter._id["$regex"] === "foobar" && output.filter._id["$options"] === "i", "Wrong id type or excess fields");

        test.done();
    },
    wrongParameters: function(test) {
        test.expect(6);

        var tagQueryWrapper = require("./../../../routes/queries/tags")();
        var input = {_id: "barbaz", skip: "2", order: "1", sort: "alpha", tags: ["beta", 123]};
        var output = tagQueryWrapper.showTagList(input);

        test.ok(output instanceof Object && Object.keys(output).length === 3, "Wrong type or excess fields");
        test.ok(output.skip === 2, "Invalid skip");
        test.ok(output.order === 1, "Invalid order");
        test.ok(output.filter instanceof Object && Object.keys(output.filter).length === 1, "Wrong filter type or excess fields");
        test.ok(output.filter._id instanceof Object && Object.keys(output.filter._id).length === 2, "Wrong id type or excess fields");
        test.ok(output.filter._id["$regex"] === "barbaz" && output.filter._id["$options"] === "i", "Wrong id type or excess fields");

        test.done();
    }
};