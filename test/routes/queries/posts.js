/**
 * Post wrapper tests
 */

exports.showPostList = {
    emptyQuery: function(test) {
        test.expect(4);

        var postQueryWrapper = require("./../../../routes/queries/posts")();
        var input = {};
        var output = postQueryWrapper.showPostList(input);

        test.ok(output instanceof Object && Object.keys(output).length === 3, "Wrong type or excess fields");
        test.ok(output.skip === 0, "Invalid skip");
        test.ok(output.order === 1, "Invalid order");
        test.ok(output.filter instanceof Object && Object.keys(output.filter).length === 0, "Wrong filter type or excess fields");

        test.done();
    },
    negativeSkip: function(test) {
        test.expect(1);

        var postQueryWrapper = require("./../../../routes/queries/posts")();
        var input = {skip: "-11"};

        test.throws(function() {
            postQueryWrapper.showPostList(input);
        }, function(err) {
            return err.message === "Page value must be integer";
        }, "Page number shouldn't be less than 0");

        test.done();
    },
    nonNumberSkip: function(test) {
        test.expect(1);

        var postQueryWrapper = require("./../../../routes/queries/posts")();
        var input = {skip: ["11", "1123"]};

        test.throws(function() {
            postQueryWrapper.showPostList(input);
        }, function(err) {
            return err.message === "Page value must be integer";
        }, "Page number should be integer");

        test.done();
    },
    validSkip: function(test) {
        test.expect(4);

        var postQueryWrapper = require("./../../../routes/queries/posts")();
        var input = {skip: "34"};
        var output = postQueryWrapper.showPostList(input);

        test.ok(output instanceof Object && Object.keys(output).length === 3, "Wrong type or excess fields");
        test.ok(output.skip === 34, "Invalid skip");
        test.ok(output.order === 1, "Invalid order");
        test.ok(output.filter instanceof Object && Object.keys(output.filter).length === 0, "Wrong filter type or excess fields");

        test.done();
    },
    invalidOrder: function(test) {
        test.expect(1);

        var postQueryWrapper = require("./../../../routes/queries/posts")();
        var input = {order: "3"};

        test.throws(function() {
            postQueryWrapper.showPostList(input);
        }, function(err) {
            return err.message === "Order must be integer";
        }, "Order should be 1 or -1");

        test.done();
    },
    nonNumberOrder: function(test) {
        test.expect(1);

        var postQueryWrapper = require("./../../../routes/queries/posts")();
        var input = {order: ["1", 0]};

        test.throws(function() {
            postQueryWrapper.showPostList(input);
        }, function(err) {
            return err.message === "Order must be integer";
        }, "Order should be 1 or -1");

        test.done();
    },
    validOrder: function(test) {
        test.expect(4);

        var postQueryWrapper = require("./../../../routes/queries/posts")();
        var input = {order: "-1"};
        var output = postQueryWrapper.showPostList(input);

        test.ok(output instanceof Object && Object.keys(output).length === 3, "Wrong type or excess fields");
        test.ok(output.skip === 0, "Invalid skip");
        test.ok(output.order === -1, "Invalid order");
        test.ok(output.filter instanceof Object && Object.keys(output.filter).length === 0, "Wrong filter type or excess fields");

        test.done();
    },
    invalidBlogs: function(test) {
        test.expect(4);

        var postQueryWrapper = require("./../../../routes/queries/posts")();
        [
            {blogs: [123]},
            {blogs: [false]},
            {blogs: [{a: "bfde"}]},
            {blogs: [[2, 4], [5, 6]]}
        ].forEach(function(input) {
            test.throws(function() {
                postQueryWrapper.showPostList(input);
            }, function(err) {
                return err.message === "Invalid blog value";
            }, "Blog search query must be string");
        });

        test.done();
    },
    nonArrayBlogs: function(test) {
        test.expect(4);

        var postQueryWrapper = require("./../../../routes/queries/posts")();
        [
            {blogs: 123},
            {blogs: false},
            {blogs: {a: "bfde"}},
            {blogs: "[[2, 4], [5, 6]]"}
        ].forEach(function(input) {
            test.throws(function() {
                postQueryWrapper.showPostList(input);
            }, function(err) {
                return err.message === "blogs value must be array";
            }, "Blogs value must be array");
        });

        test.done();
    },
    validBlogs: function(test) {
        test.expect(12);

        var postQueryWrapper = require("./../../../routes/queries/posts")();
        var input = {blogs: ["^aa", "^qqq$", "weee"]};
        var output = postQueryWrapper.showPostList(input);

        test.ok(output instanceof Object && Object.keys(output).length === 3, "Wrong type or excess fields");
        test.ok(output.skip === 0, "Invalid skip");
        test.ok(output.order === 1, "Invalid order");
        test.ok(output.filter instanceof Object && Object.keys(output.filter).length === 1, "Wrong filter type or excess fields");
        test.ok(output.filter.blog instanceof Object && Object.keys(output.filter.blog).length === 1, "Wrong blog type or excess fields");
        test.ok(output.filter.blog["$in"] instanceof Array && output.filter.blog["$in"].length === input.blogs.length, "Wrong blog value");

        for (var i = 0; i < input.blogs.length; i++) {
            test.ok(output.filter.blog["$in"][i] instanceof RegExp);
            test.ok(output.filter.blog["$in"][i].toString() === ["/", input.blogs[i], "/i"].join(""));
        }

        test.done();
    },
    invalidTags: function(test) {
        test.expect(4);

        var postQueryWrapper = require("./../../../routes/queries/posts")();
        [
            {tags: [123]},
            {tags: [false]},
            {tags: [{a: "bfde"}]},
            {tags: [[2, 4], [5, 6]]}
        ].forEach(function(input) {
            test.throws(function() {
                postQueryWrapper.showPostList(input);
            }, function(err) {
                return err.message === "Invalid tag value";
            }, "Tag search query must be string");
        });

        test.done();
    },
    nonArrayTags: function(test) {
        test.expect(4);

        var postQueryWrapper = require("./../../../routes/queries/posts")();
        [
            {tags: 123},
            {tags: false},
            {tags: {a: "bfde"}},
            {tags: "[[2, 4], [5, 6]]"}
        ].forEach(function(input) {
            test.throws(function() {
                postQueryWrapper.showPostList(input);
            }, function(err) {
                return err.message === "tags value must be array";
            }, "Tags value must be array");
        });

        test.done();
    },
    validTags: function(test) {
        test.expect(23);

        var postQueryWrapper = require("./../../../routes/queries/posts")();
        var input = {tags: ["^aa", "^qqq$", "weee"]};
        var output = postQueryWrapper.showPostList(input);

        test.ok(output instanceof Object && Object.keys(output).length === 3, "Wrong type or excess fields");
        test.ok(output.skip === 0, "Invalid skip");
        test.ok(output.order === 1, "Invalid order");
        test.ok(output.filter instanceof Object && Object.keys(output.filter).length === 1, "Wrong filter type or excess fields");
        test.ok(output.filter["$or"] instanceof Array && output.filter["$or"].length === 2, "Wrong tag type or excess fields");

        test.ok(output.filter["$or"][0] instanceof Object && Object.keys(output.filter["$or"][0]).length === 1, "Wrong info.tags type or excess fields");
        test.ok(output.filter["$or"][0]['info.tags'] instanceof Object && Object.keys(output.filter["$or"][0]['info.tags']).length === 1, "Wrong info.tags type or excess fields");
        test.ok(output.filter["$or"][0]['info.tags']['$in'] instanceof Array && output.filter["$or"][0]['info.tags']['$in'].length === input.tags.length, "Wrong info.tags type or excess fields");

        test.ok(output.filter["$or"][1] instanceof Object && Object.keys(output.filter["$or"][1]).length === 1, "Wrong user_tags type or excess fields");
        test.ok(output.filter["$or"][1]['user_tags'] instanceof Object && Object.keys(output.filter["$or"][1]['user_tags']).length === 1, "Wrong user_tags type or excess fields");
        test.ok(output.filter["$or"][1]['user_tags']['$in'] instanceof Array && output.filter["$or"][1]['user_tags']['$in'].length === input.tags.length, "Wrong user_tags type or excess fields");

        for (var i = 0; i < input.tags.length; i++) {
            test.ok(output.filter["$or"][0]['info.tags']['$in'][i] instanceof RegExp);
            test.ok(output.filter["$or"][0]['info.tags']['$in'][i].toString() === ["/", input.tags[i], "/i"].join(""));

            test.ok(output.filter["$or"][1]['user_tags']['$in'][i] instanceof RegExp);
            test.ok(output.filter["$or"][1]['user_tags']['$in'][i].toString() === ["/", input.tags[i], "/i"].join(""));
        }

        test.done();
    },
    invalidLikes: function(test) {
        test.expect(4);

        var postQueryWrapper = require("./../../../routes/queries/posts")();
        [
            {liked: [1]},
            {liked: [false]},
            {liked: [["1"]]},
            {liked: [{"1": "0"}]}
        ].forEach(function(input) {
            test.throws(function() {
                postQueryWrapper.showPostList(input);
            }, function(err) {
                return err.message === "Invalid like value";
            }, "Like search query must be array of 1 and 0");
        });

        test.done();
    },
    nonArrayLikes: function(test) {
        test.expect(4);

        var postQueryWrapper = require("./../../../routes/queries/posts")();
        [
            {liked: 123},
            {liked: false},
            {liked: {a: "bfde"}},
            {liked: "[[2, 4], [5, 6]]"}
        ].forEach(function(input) {
            test.throws(function() {
                postQueryWrapper.showPostList(input);
            }, function(err) {
                return err.message === "liked value must be array";
            }, "Likes value must be array");
        });

        test.done();
    },
    validLikes: function(test) {
        test.expect(8);

        var postQueryWrapper = require("./../../../routes/queries/posts")();
        var input = {liked: ["0", "1"]};
        var output = postQueryWrapper.showPostList(input);

        test.ok(output instanceof Object && Object.keys(output).length === 3, "Wrong type or excess fields");
        test.ok(output.skip === 0, "Invalid skip");
        test.ok(output.order === 1, "Invalid order");
        test.ok(output.filter instanceof Object && Object.keys(output.filter).length === 1, "Wrong filter type or excess fields");
        test.ok(output.filter["user_liked"] instanceof Object && Object.keys(output.filter["user_liked"]).length === 1, "Wrong user_liked type or excess fields");
        test.ok(output.filter["user_liked"]["$in"] instanceof Array && output.filter["user_liked"]["$in"].length === 2, "Wrong user_liked type or excess fields");

        test.ok(output.filter["user_liked"]["$in"][0] === false, "Invalid like value");
        test.ok(output.filter["user_liked"]["$in"][1] === true, "Invalid like value");

        test.done();
    },
    invalidTypes: function(test) {
        test.expect(6);

        var postQueryWrapper = require("./../../../routes/queries/posts")();
        [
            {types: ["Answer"]},
            {types: [["answer"]]},
            {types: [1]},
            {types: [false]},
            {types: [["1"]]},
            {types: [{"1": "0"}]}
        ].forEach(function(input) {
            test.throws(function() {
                postQueryWrapper.showPostList(input);
            }, function(err) {
                return err.message === "Invalid type value";
            }, "Type search query must be array");
        });

        test.done();
    },
    nonArrayTypes: function(test) {
        test.expect(4);

        var postQueryWrapper = require("./../../../routes/queries/posts")();
        [
            {types: 123},
            {types: false},
            {types: {a: "bfde"}},
            {types: "[[2, 4], [5, 6]]"}
        ].forEach(function(input) {
            test.throws(function() {
                postQueryWrapper.showPostList(input);
            }, function(err) {
                return err.message === "types value must be array";
            }, "Type search query must be array");
        });

        test.done();
    },
    validTypes: function(test) {
        test.expect(9);

        var postQueryWrapper = require("./../../../routes/queries/posts")();
        var input = {types: ["answer", "photo", "quote"]};
        var output = postQueryWrapper.showPostList(input);

        test.ok(output instanceof Object && Object.keys(output).length === 3, "Wrong type or excess fields");
        test.ok(output.skip === 0, "Invalid skip");
        test.ok(output.order === 1, "Invalid order");
        test.ok(output.filter instanceof Object && Object.keys(output.filter).length === 1, "Wrong filter type or excess fields");
        test.ok(output.filter["info.type"] instanceof Object && Object.keys(output.filter["info.type"]).length === 1, "Wrong type type or excess fields");
        test.ok(output.filter["info.type"]["$in"] instanceof Array && output.filter["info.type"]["$in"].length === input.types.length, "Wrong type type or excess fields");

        for (var i = 0; i < input.types.length; ++i) {
            test.ok(output.filter["info.type"]["$in"][i] === input.types[i], "Invalid type value");
        }

        test.done();
    },
    allIn: function(test) {
        test.expect(35);

        var postQueryWrapper = require("./../../../routes/queries/posts")();
        var input = {
            order: "-1",
            skip: "15",
            blogs: ["aaa", "^vv$", "qwe"],
            tags: ["a", "kitties"],
            liked: ["0"],
            types: ["text", "link", "chat"]
        };
        var output = postQueryWrapper.showPostList(input);

        // Common
        test.ok(output instanceof Object && Object.keys(output).length === 3, "Wrong type or excess fields");
        test.ok(output.skip === 15, "Invalid skip");
        test.ok(output.order === -1, "Invalid order");
        test.ok(output.filter instanceof Object && Object.keys(output.filter).length === 4, "Wrong filter type or excess fields");

        // Blogs
        test.ok(output.filter.blog instanceof Object && Object.keys(output.filter.blog).length === 1, "Wrong blog type or excess fields");
        test.ok(output.filter.blog["$in"] instanceof Array && output.filter.blog["$in"].length === input.blogs.length, "Wrong blog value");

        for (var i = 0; i < input.blogs.length; i++) {
            test.ok(output.filter.blog["$in"][i] instanceof RegExp);
            test.ok(output.filter.blog["$in"][i].toString() === ["/", input.blogs[i], "/i"].join(""));
        }

        // Tags
        test.ok(output.filter["$or"] instanceof Array && output.filter["$or"].length === 2, "Wrong tag type or excess fields");

        test.ok(output.filter["$or"][0] instanceof Object && Object.keys(output.filter["$or"][0]).length === 1, "Wrong info.tags type or excess fields");
        test.ok(output.filter["$or"][0]['info.tags'] instanceof Object && Object.keys(output.filter["$or"][0]['info.tags']).length === 1, "Wrong info.tags type or excess fields");
        test.ok(output.filter["$or"][0]['info.tags']['$in'] instanceof Array && output.filter["$or"][0]['info.tags']['$in'].length === input.tags.length, "Wrong info.tags type or excess fields");

        test.ok(output.filter["$or"][1] instanceof Object && Object.keys(output.filter["$or"][1]).length === 1, "Wrong user_tags type or excess fields");
        test.ok(output.filter["$or"][1]['user_tags'] instanceof Object && Object.keys(output.filter["$or"][1]['user_tags']).length === 1, "Wrong user_tags type or excess fields");
        test.ok(output.filter["$or"][1]['user_tags']['$in'] instanceof Array && output.filter["$or"][1]['user_tags']['$in'].length === input.tags.length, "Wrong user_tags type or excess fields");

        for (var i = 0; i < input.tags.length; i++) {
            test.ok(output.filter["$or"][0]['info.tags']['$in'][i] instanceof RegExp);
            test.ok(output.filter["$or"][0]['info.tags']['$in'][i].toString() === ["/", input.tags[i], "/i"].join(""));

            test.ok(output.filter["$or"][1]['user_tags']['$in'][i] instanceof RegExp);
            test.ok(output.filter["$or"][1]['user_tags']['$in'][i].toString() === ["/", input.tags[i], "/i"].join(""));
        }

        // Likes
        test.ok(output.filter["user_liked"] instanceof Object && Object.keys(output.filter["user_liked"]).length === 1, "Wrong user_liked type or excess fields");
        test.ok(output.filter["user_liked"]["$in"] instanceof Array && output.filter["user_liked"]["$in"].length === 1, "Wrong user_liked type or excess fields");

        test.ok(output.filter["user_liked"]["$in"][0] === false, "Invalid like value");

        // Types
        test.ok(output.filter["info.type"] instanceof Object && Object.keys(output.filter["info.type"]).length === 1, "Wrong type type or excess fields");
        test.ok(output.filter["info.type"]["$in"] instanceof Array && output.filter["info.type"]["$in"].length === input.types.length, "Wrong type type or excess fields");

        for (var i = 0; i < input.types.length; ++i) {
            test.ok(output.filter["info.type"]["$in"][i] === input.types[i], "Invalid type value");
        }


        test.done();
    },
    wrongParams: function(test) {
        test.expect(29);

        var postQueryWrapper = require("./../../../routes/queries/posts")();
        var input = {
            order: "1",
            skip: "2",
            blogs: ["bbb", "tt|dd"],
            tags: ["fluffy"],
            liked: ["1"],
            types: ["photo", "audio", "video"],
            _id: "123",
            date: "tomorrow",
        };
        var output = postQueryWrapper.showPostList(input);

        // Common
        test.ok(output instanceof Object && Object.keys(output).length === 3, "Wrong type or excess fields");
        test.ok(output.skip === 2, "Invalid skip");
        test.ok(output.order === 1, "Invalid order");
        test.ok(output.filter instanceof Object && Object.keys(output.filter).length === 4, "Wrong filter type or excess fields");

        // Blogs
        test.ok(output.filter.blog instanceof Object && Object.keys(output.filter.blog).length === 1, "Wrong blog type or excess fields");
        test.ok(output.filter.blog["$in"] instanceof Array && output.filter.blog["$in"].length === input.blogs.length, "Wrong blog value");

        for (var i = 0; i < input.blogs.length; i++) {
            test.ok(output.filter.blog["$in"][i] instanceof RegExp);
            test.ok(output.filter.blog["$in"][i].toString() === ["/", input.blogs[i], "/i"].join(""));
        }

        // Tags
        test.ok(output.filter["$or"] instanceof Array && output.filter["$or"].length === 2, "Wrong tag type or excess fields");

        test.ok(output.filter["$or"][0] instanceof Object && Object.keys(output.filter["$or"][0]).length === 1, "Wrong info.tags type or excess fields");
        test.ok(output.filter["$or"][0]['info.tags'] instanceof Object && Object.keys(output.filter["$or"][0]['info.tags']).length === 1, "Wrong info.tags type or excess fields");
        test.ok(output.filter["$or"][0]['info.tags']['$in'] instanceof Array && output.filter["$or"][0]['info.tags']['$in'].length === input.tags.length, "Wrong info.tags type or excess fields");

        test.ok(output.filter["$or"][1] instanceof Object && Object.keys(output.filter["$or"][1]).length === 1, "Wrong user_tags type or excess fields");
        test.ok(output.filter["$or"][1]['user_tags'] instanceof Object && Object.keys(output.filter["$or"][1]['user_tags']).length === 1, "Wrong user_tags type or excess fields");
        test.ok(output.filter["$or"][1]['user_tags']['$in'] instanceof Array && output.filter["$or"][1]['user_tags']['$in'].length === input.tags.length, "Wrong user_tags type or excess fields");

        for (var i = 0; i < input.tags.length; i++) {
            test.ok(output.filter["$or"][0]['info.tags']['$in'][i] instanceof RegExp);
            test.ok(output.filter["$or"][0]['info.tags']['$in'][i].toString() === ["/", input.tags[i], "/i"].join(""));

            test.ok(output.filter["$or"][1]['user_tags']['$in'][i] instanceof RegExp);
            test.ok(output.filter["$or"][1]['user_tags']['$in'][i].toString() === ["/", input.tags[i], "/i"].join(""));
        }

        // Likes
        test.ok(output.filter["user_liked"] instanceof Object && Object.keys(output.filter["user_liked"]).length === 1, "Wrong user_liked type or excess fields");
        test.ok(output.filter["user_liked"]["$in"] instanceof Array && output.filter["user_liked"]["$in"].length === 1, "Wrong user_liked type or excess fields");

        test.ok(output.filter["user_liked"]["$in"][0] === true, "Invalid like value");

        // Types
        test.ok(output.filter["info.type"] instanceof Object && Object.keys(output.filter["info.type"]).length === 1, "Wrong type type or excess fields");
        test.ok(output.filter["info.type"]["$in"] instanceof Array && output.filter["info.type"]["$in"].length === input.types.length, "Wrong type type or excess fields");

        for (var i = 0; i < input.types.length; ++i) {
            test.ok(output.filter["info.type"]["$in"][i] === input.types[i], "Invalid type value");
        }


        test.done();
    }

};


exports.like = {
    emptyQuery: function(test) {
        test.expect(1);

        var postQueryWrapper = require("./../../../routes/queries/posts")();
        var input = {};

        test.throws(function() {
            postQueryWrapper.like(input);
        }, function(err) {
            return err.message === "Post id undefined";
        }, "Query must have _id and like values");

        test.done();
    },
    invalidId: function(test) {
        test.expect(4);

        var postQueryWrapper = require("./../../../routes/queries/posts")();

        [
            {_id: "dssdf"},
            {_id: 11},
            {_id: false},
            {_id: ["34"]},
        ].forEach(function(input) {
            test.throws(function() {
                postQueryWrapper.like(input);
            }, function(err) {
                return err.message === "Post id must be integer";
            }, "Query must have _id number value as string");
        });

        test.done();
    },
    invalidLike: function(test) {
        test.expect(4);

        var postQueryWrapper = require("./../../../routes/queries/posts")();

        [
            {_id: "123", like: false},
            {_id: "123", like: ["0"]},
            {_id: "123", like: 1},
            {_id: "123", like: {0: 1}},
        ].forEach(function(input) {
            test.throws(function() {
                postQueryWrapper.like(input);
            }, function(err) {
                return err.message === "Post like has invalid value";
            }, "Like must 0 or 1");
        });
        test.done();
    },
    validQuery: function(test) {
        test.expect(3);

        var postQueryWrapper = require("./../../../routes/queries/posts")();
        var input = {_id: "55", like: "1"};
        var output = postQueryWrapper.like(input);

        test.ok(output instanceof Object && Object.keys(output).length === 2, "Invalid type or excess fields");
        test.ok(output._id === 55, "Invalid _id value");
        test.ok(output.user_liked === 1, "Invalid like value");

        test.done();
    },
    wrongParams: function(test) {
        test.expect(3);

        var postQueryWrapper = require("./../../../routes/queries/posts")();
        var input = {_id: "888", like: "0", order: "1", skip: "16"};
        var output = postQueryWrapper.like(input);

        test.ok(output instanceof Object && Object.keys(output).length === 2, "Invalid type or excess fields");
        test.ok(output._id === 888, "Invalid _id value");
        test.ok(output.user_liked === 0, "Invalid like value");

        test.done();
    }
};


exports.userTag = {
    emptyQuery: function(test) {
        test.expect(1);

        var postQueryWrapper = require("./../../../routes/queries/posts")();
        var input = {};

        test.throws(function() {
            postQueryWrapper.userTag(input);
        }, function(err) {
            return err.message === "Post id is undefined";
        }, "Query must have _id and like values");

        test.done();
    },
    invalidId: function(test) {
        test.expect(4);

        var postQueryWrapper = require("./../../../routes/queries/posts")();

        [
            {_id: "dssdf"},
            {_id: 11},
            {_id: false},
            {_id: ["34"]},
        ].forEach(function(input) {
            test.throws(function() {
                postQueryWrapper.userTag(input);
            }, function(err) {
                return err.message === "Post id must be integer";
            }, "Query must have _id number value as string");
        });

        test.done();
    },
    invalidTag: function(test) {
        test.expect(4);

        var postQueryWrapper = require("./../../../routes/queries/posts")();

        [
            {_id: "123", tag: false},
            {_id: "123", tag: ["0"]},
            {_id: "123", tag: 1},
            {_id: "123", tag: {0: 1}},
        ].forEach(function(input) {
            test.throws(function() {
                postQueryWrapper.userTag(input);
            }, function(err) {
                return err.message === "Tag has invalid value";
            }, "Tag must be string");
        });
        test.done();
    },
    invalidOperation: function(test) {
        test.expect(4);

        var postQueryWrapper = require("./../../../routes/queries/posts")();

        [
            {_id: "123", tag: "tag", add: 1},
            {_id: "123", tag: "tag", add: 0},
            {_id: "123", tag: "tag", add: ["1"]},
            {_id: "123", tag: "tag", add: [0]},
        ].forEach(function(input) {
            test.throws(function() {
                postQueryWrapper.userTag(input);
            }, function(err) {
                return err.message === "Tag operation has invalid value";
            }, "Tag operation must be 0 or 1");
        });
        test.done();
    },
    validQuery: function(test) {
        test.expect(4);

        var postQueryWrapper = require("./../../../routes/queries/posts")();
        var input = {_id: "55", tag: "sssss", add: "1"};
        var output = postQueryWrapper.userTag(input);

        test.ok(output instanceof Object && Object.keys(output).length === 3, "Invalid type or excess fields");
        test.ok(output._id === 55, "Invalid _id value");
        test.ok(output.tag === "sssss", "Invalid tag value");
        test.ok(output.add === 1, "Invalid tag operation value");

        test.done();
    },
    wrongParams: function(test) {
        test.expect(4);

        var postQueryWrapper = require("./../../../routes/queries/posts")();
        var input = {_id: "22", tag: "bgbgbgbgg", add: "0", order: "1", skip: "12232"};
        var output = postQueryWrapper.userTag(input);

        test.ok(output instanceof Object && Object.keys(output).length === 3, "Invalid type or excess fields");
        test.ok(output._id === 22, "Invalid _id value");
        test.ok(output.tag === "bgbgbgbgg", "Invalid tag value");
        test.ok(output.add === 0, "Invalid tag operation value");

        test.done();
    }
};