/**
 * Queries wrappers tests
 */

exports.blogs = require("./blogs");
exports.posts = require("./posts");
exports.queryWrapper = require("./queryWrapper");
exports.sessions = require("./sessions");
exports.tags = require("./tags");
