var RSS = require("rss");

/**
 * Wrapper to prepare object for posts collection
 *
 * @param {Object} config
 * @returns {BlogsWrapper}
 */
var PostsWrapper = function(config) {
    this.config = config;

    this.feedWrapStrategy = null;

    return this;
};

PostsWrapper.prototype.__preload = function() {
    console.log('Preloading posts wrapper');

    this.getFeedWrapStrategy();
};

/**
 * Complementing object with missing parameters before inserting and output
 *
 * @param {Object} item
 * @returns {Object}
 */
PostsWrapper.prototype.complementItem = function(item) {
    if (typeof item.user_tags === "undefined") {
        item.user_tags = [];
    }

    if (typeof item.user_liked === "undefined") {
        item.user_liked = false;
    }

    if (typeof item.info === "undefined") {
        item.info = false;
    }

    if (typeof item.blog === "undefined") {
        item.blog = false;
    }

    if (typeof item.processed === "undefined") {
        item.processed = false;
    }

    return item;
};

/**
 * Prepare object to be outputted
 *
 * @param {Object} item
 * @returns {Object}
 */
PostsWrapper.prototype.prepareItemForOutput = function(item) {
    item = this.complementItem(item);

    return item;
};

PostsWrapper.prototype.getFeedWrapStrategy = function() {
    this.feedWrapStrategy = this.feedWrapStrategy || {
        text: function(doc, item) {
            item.title = doc.info.title || "Post";
            item.description = doc.info.body;

            return item;
        },
        link: function(doc, item) {
            item.title = doc.info.title || "Link";
            item.description = '<div><a href="' + doc.info.url + '">' + (doc.info.title || doc.info.url) + '</a></div><div>' + (doc.info.description || "") + '</div>';

            return item;
        },
        quote: function(doc, item) {
            item.title = "Quote";
            item.description = '<div><blockquote>' + (doc.info.text || "") + '</blockquote></div><div>' + (doc.info.source || "") + '</div>';

            return item;
        },
        answer: function(doc, item) {
            item.title = "Answer";

            item.description = '<div><blockquote>';
            if (doc.info.asking_url) {
                item.description += '<a href="' + doc.info.asking_url + '">' + doc.info.asking_name + '</a>';
            }
            else {
                item.description += doc.info.asking_name;
            }
            item.description += ' asked: ' + doc.info.question + '</blockquote></div><div>' + (doc.info.answer || "") + '</div>';

            return item;
        },
        chat: function(doc, item) {
            item.title = doc.info.title || "Chat";
            item.description = doc.info.dialogue.map(function(line) {
                return "<p><b>" + line.label + "</b> " + line.phrase + "</p>";
            }).join("");

            return item;
        },
        audio: function(doc, item) {
            item.title = "Audio";
            item.description = "<div>" + doc.info.embed + "</div><div>" + (doc.info.caption || "") + "</div>";

            return item;
        },
        video: function(doc, item) {
            item.title = "Video";
            item.description = "<div>" + doc.info.player[doc.info.player.length - 1].embed_code + "</div><div>" + (doc.info.caption || "") + "</div>";

            return item;
        },
        photo: function(doc, item) {
            item.title = "Photo";
            item.description = "<div>";

            var odd = false;
            doc.info.photos.forEach(function(photo) {
                var currentPhoto = null;
                photo.alt_sizes.forEach(function(altPhoto) {
                    if (altPhoto.width <= 250 && (!currentPhoto || altPhoto.width > currentPhoto.width)) {
                        currentPhoto = altPhoto;
                    }
                });
                currentPhoto = currentPhoto || photo.original_size;

                item.description += '<a href="' + photo.original_size.url + '"><img src="' + currentPhoto.url + '"/></a> ';

                if (odd) {
                    item.description += "<br>";
                }
                odd = !odd;
            });

            item.description += "</div><div>" + (doc.info.caption || "") + "</div>";

            return item;
        }
    };

    return this.feedWrapStrategy;
};

PostsWrapper.prototype.wrapForFeed = function(doc) {
    var item = {
        title: "title",
        description: "content",
        url: doc.info.post_url,
        date: new Date(doc.info.date),
        author: doc.info.blog_name
    };

    if (this.getFeedWrapStrategy()[doc.info.type]) {
        item = this.getFeedWrapStrategy()[doc.info.type](doc, item);
    }

    return item;
};

/**
 * Export wrapper, returns render function
 *
 * @param {type} type
 * @param {type} docs
 * @returns {Function}
 */
PostsWrapper.prototype.export = function(docs) {
    var self = this;

    var feedSettings = {};

    [
        {field: 'title', value: this.config.posts_export.title},
        {field: 'site_url', value: this.config.posts_export.site_url},
        {field: 'feed_url', value: this.config.posts_export.feed_url},
        {field: 'description', value: this.config.posts_export.description},
        {field: 'author', value: this.config.posts_export.author}
    ].forEach(function(item) {
        if (item.value) {
            feedSettings[item.field] = item.value;
        }
    });

    var feed = new RSS(feedSettings);
    docs.forEach(function(doc) {
        feed.item(self.wrapForFeed(doc));
    });

    return function() {
        return feed.xml();
    };
};

var wrapper = null;
module.exports = function(config) {
    wrapper = wrapper || new PostsWrapper(config);
    return wrapper;
};