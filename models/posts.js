/**
 * Posts logic
 *
 * @param {DaoFactory} daoFactory
 * @param {TaskManager} taskManager
 * @param {Object} config
 * @returns {PostsModel}
 */
var PostsModel = function(daoFactory, taskManager, config) {
    this.daoFactory = daoFactory;
    this.taskManager = taskManager;
    this.config = config;

    this.postsWrapper = null;
    this.tumblrApi = null;

    this.readPage = null;
    this.finishRead = null;

    this.updatePost = null;

    this.exportCache = {
        lastTime: 0,
        lastDocs: []
    };

    return this;
};

/**
 * Autoloader
 *
 * @returns {undefined}
 */
PostsModel.prototype.__preload = function() {
    console.log('Preloading posts model');

    this.getPostsWrapper();
    this.getTumblrApi();

    this.getReadPage();
    this.getFinishRead();
};

/**
 * Getting tumblr api object
 *
 * @returns {TumblrApi}
 */
PostsModel.prototype.getTumblrApi = function() {
    this.tumblrApi = this.tumblrApi || require("./../helpers/tumblrApi")(this.config);
    return this.tumblrApi;
};

/**
 * Getting wrapper for posts collection
 *
 * @returns {PostsWrapper}
 */
PostsModel.prototype.getPostsWrapper = function() {
    this.postsWrapper = this.postsWrapper || require("./wrappers/posts")(this.config);
    return this.postsWrapper;
};

/**
 * Finish reading blog - update user data
 *
 * @returns {PostsModel.finishRead}
 */
PostsModel.prototype.getFinishRead = function() {
    if (this.finishRead === null) {
        var self = this;

        this.finishRead = function(blog, postMaxtimestamp, finishTask) {
            self.daoFactory.getBlogsDao().updateSingleDoc({
                _id: blog,
                last_read: {$lt: postMaxtimestamp}
            }, {
                '$set': {last_read: postMaxtimestamp}
            }, function(err) {
                finishTask();
            });
        };
    }

    return this.finishRead;
};

/**
 * Read a single page from blog
 *
 * @returns {PostsModel.readPage}
 */
PostsModel.prototype.getReadPage = function() {
    if (this.readPage === null) {
        var self = this;

        this.readPage = function(taskData, finishTask) {
            self.getTumblrApi().getPosts(taskData.blog, taskData.offset, function(err, posts) {
                if (err) {
                    console.dir(err);
                    finishTask();
                    return;
                }

                if (!posts) {
                    console.log("An error occurred");
                    finishTask();
                    return;
                }

                try {
                    var postMaxTimestamp = taskData.post_max_timestamp;
                    if (posts.length === 0) {
                        return self.getFinishRead()(taskData.blog, postMaxTimestamp, finishTask);
                    }

                    var needToStop = false;

                    posts.forEach(function(post) {
                        if (post.timestamp > postMaxTimestamp) {
                            postMaxTimestamp = post.timestamp;
                        }
                        if (post.timestamp <= taskData.last_read) {
                            needToStop = true;
                        }
                    });

                    self.daoFactory.getPostsDao().upsertManyDocsById(posts.map(function(item) {
                        return self.getPostsWrapper().complementItem({_id: item.id, info: item, blog: item.blog_name + ".tumblr.com"});
                    }), function() {
                        if (needToStop) {
                            return self.getFinishRead()(taskData.blog, postMaxTimestamp, finishTask);
                        }

                        self.taskManager.addTask({
                            blog: taskData['blog'],
                            offset: taskData['offset'] + self.config.read_pages.limit,
                            last_read: taskData['last_read'],
                            post_max_timestamp: postMaxTimestamp
                        }, self.getReadPage());

                        finishTask();
                    });
                }
                catch (err) {
                    console.dir(err);
                    finishTask();
                }
            });
        };
    }

    return this.readPage;
};

/**
 * Main function get extract posts from blogs
 *
 * @param {Function} callback
 * @returns {undefined}
 */
PostsModel.prototype.startRead = function(callback) {
    var self = this;

    this.daoFactory.getBlogsDao().getForRead(function(doc) {
        console.log('Starting reading for ', doc['_id'], ' last read ', doc['last_read']);

        self.taskManager.addTask({
            blog: doc['_id'],
            offset: 0,
            last_read: doc['last_read'],
            post_max_timestamp: 0
        }, self.getReadPage());
    }, function(err) {
        return callback(err);
    });
};

/**
 * Getting documents for output
 *
 * @param {Object} input
 * @param {Function} callback
 * @returns {undefined}
 */
PostsModel.prototype.getForList = function(input, callback) {
    var self = this;

    this.daoFactory.getPostsDao().getByQuery(input.filter, [['_id', input.order]], input.skip * self.config.pagination.postslimit, self.config.pagination.postslimit,
            function(err, docs, count) {
                if (err) {
                    return callback(err, null, null, null);
                }

                return callback(err, docs.map(self.getPostsWrapper().prepareItemForOutput, self.getPostsWrapper()), (input.skip + 1) * self.config.pagination.postslimit < count, count);
            });
};

/**
 * Add or remove like to document
 *
 * @param {Object} input
 * @param {Function} callback
 * @returns {undefined}
 */
PostsModel.prototype.like = function(input, callback) {
    this.daoFactory.getPostsDao().updateSingleDoc({_id: input._id}, {"$set": {"user_liked": input.user_liked ? true : false}},
    function(err) {
        if (err) {
            return callback(err);
        }

        return callback(null);
    });
};

/**
 * Add or remove user tag to document
 *
 * @param {Object} input
 * @param {Function} callback
 * @returns {undefined}
 */
PostsModel.prototype.userTag = function(input, callback) {
    var update;
    if (input.add) {
        update = {$addToSet: {"user_tags": input.tag}};
    }
    else {
        update = {$pull: {"user_tags": input.tag}};
    }

    this.daoFactory.getPostsDao().updateSingleDoc({_id: input._id}, update,
            function(err) {
                if (err) {
                    return callback(err);
                }

                return callback(null);
            });
};

/**
 * Get posts for export
 *
 * @param {Function} callback
 * @returns {undefined}
 */
PostsModel.prototype.export = function(callback) {
    var self = this;

    if ((Date.now() - self.exportCache.lastTime) / 1000. > self.config.posts_export.caching) {
        this.daoFactory.getPostsDao().getByQuery({}, [['_id', -1]], 0, self.config.posts_export.limit,
                function(err, docs) {
                    if (err) {
                        return callback(err, null);
                    }

                    if ((Date.now() - self.exportCache.lastTime) / 1000. > self.config.posts_export.caching) {
                        self.exportCache.lastTime = Date.now();
                        self.exportCache.lastDocs = docs;
                    }

                    return callback(null, self.getPostsWrapper().export(docs));
                });
    }
    else {
        return callback(null, self.getPostsWrapper().export(self.exportCache.lastDocs));
    }
};

/**
 * Update a single post
 *
 * @returns {PostsModel.updatePost}
 */
PostsModel.prototype.getUpdatePost = function() {
    if (this.updatePost === null) {
        var self = this;

        this.updatePost = function(taskData, finishTask) {
            self.getTumblrApi().getPostById(taskData.blog, taskData.id, function(err, posts) {
                if (err) {
                    console.dir(err);
                    finishTask();
                    return;
                }

                if (!posts) {
                    console.log("Post ", taskData.id, " from blog ", taskData.blog, " not found");
                    finishTask();
                    return;
                }

                if (posts.length === 0) {
                    finishTask();
                    return;
                }

                self.daoFactory.getPostsDao().upsertManyDocsById(posts.map(function(item) {
                    return self.getPostsWrapper().complementItem({_id: item.id, info: item, blog: taskData.blog});
                }), function() {
                    finishTask();
                });
            });
        };
    }

    return this.updatePost;
}

/**
 * Main function for mass updating posts
 *
 * @param {Function} callback
 * @returns {undefined}
 */
PostsModel.prototype.startUpdatePosts = function(callback) {
    var self = this;

    this.daoFactory.getPostsDao().getByQueryIterative({}, null, null, null, function(doc) {
        console.log('Starting update post ', doc['_id'], ' for blog ', doc['info']['blog_name']);

        self.taskManager.addTask({
            blog: doc['info']['blog_name'] + ".tumblr.com",
            id: doc['_id']
        }, self.getUpdatePost());
    }, function(err) {
        return callback(err);
    });
};

module.exports = function() {
    var o = Object.create(PostsModel.prototype);
    return PostsModel.apply(o, arguments);
};