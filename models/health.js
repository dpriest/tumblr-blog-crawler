var moment = require("moment"); //For date routine

/**
 * Health logic
 *
 * @param {TaskManager} taskManager
 * @param {Object} config
 * @returns {HealthModel}
 */
var HealthModel = function(taskManager, config) {
    this.taskManager = taskManager;
    this.config = config;

    return this;
};

HealthModel.prototype.formatMemory = function(value) {
    return value + " bytes (" + Math.round((value + 0.) / (1024 * 1024.)) + " mb)";
};

HealthModel.prototype.copy = function(object) {
    var result = {};
    for (var key in object) {
        if (object.hasOwnProperty(key)) {
            result[key] = object[key];
        }
    }

    return result;
};

HealthModel.prototype.getHealth = function(callback) {
    try {
        var result = {
            taskManager: {
                tasks: this.taskManager.query.length,
                working: this.taskManager.working
            }
        };

        callback(null, result);
    }
    catch (err) {
        callback(err);
    }
};

module.exports = function() {
    var o = Object.create(HealthModel.prototype);
    return HealthModel.apply(o, arguments);
};