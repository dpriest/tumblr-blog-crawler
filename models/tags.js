/**
 * Tags logic
 *
 * @param {DaoFactory} daoFactory
 * @param {TaskManager} taskManager
 * @param {Object} config
 * @returns {TagsModel}
 */
var TagsModel = function(daoFactory, taskManager, config) {
    this.daoFactory = daoFactory;
    this.taskManager = taskManager;
    this.config = config;

    this.blogsWrapper = null;
    this.tumblrApi = null;
    this.parser = null;

    this.processSinglePage = null;

    return this;
};

/**
 * Autoloader
 *
 * @returns {undefined}
 */
TagsModel.prototype.__preload = function() {
    console.log('Preloading tags model');

    this.getBlogsWrapper();
    this.getTumblrApi();
    this.getParser();

    this.getProcessSinglePage();
};

/**
 * Getting tumblr api object
 *
 * @returns {TumblrApi}
 */
TagsModel.prototype.getTumblrApi = function() {
    this.tumblrApi = this.tumblrApi || require("./../helpers/tumblrApi")(this.config);
    return this.tumblrApi;
};

/**
 * Getting parser object
 *
 * @returns {Parser}
 */
TagsModel.prototype.getParser = function() {
    this.parser = this.parser || require("./../helpers/parser")(this.config);
    return this.parser;
};

/**
 * Getting wrapper for blogs collection
 *
 * @returns {BlogsWrapper}
 */
TagsModel.prototype.getBlogsWrapper = function() {
    this.blogsWrapper = this.blogsWrapper || require("./wrappers/blogs")(this.config);
    return this.blogsWrapper;
};

/**
 * Process single page with tags
 *
 * @returns {TagsModel.processSinglePage}
 */
TagsModel.prototype.getProcessSinglePage = function() {
    if (this.processSinglePage === null) {
        var self = this;

        this.processSinglePage = function(taskData, finishTask) {
            console.log("Processing tagged page, tag ", taskData.tag, " timestamp ", taskData.before, " page ", taskData.page);

            self.getTumblrApi().getTaggedPosts(taskData.tag, taskData.before, function(err, data) {
                if (err) {
                    console.dir(err);
                    finishTask();
                    return;
                }

                try {
                    var items = [];
                    var before = 0;
                    data.forEach(function(item) {
                        items.push({_id: self.getParser().prepareLink(item['post_url'])});

                        var timestamp = item.featured_timestamp || item.timestamp;
                        if (before === 0 || timestamp < before)
                            before = timestamp;
                    });

                    if (items.length === 0) {
                        return finishTask();
                    }

                    self.daoFactory.getBlogsDao().insertManyDocs(items.map(self.getBlogsWrapper().complementItem), function() {
                        if (taskData.page < self.config.tagged_pages.pages) {
                            self.taskManager.addTask(
                                    {
                                        tag: taskData.tag,
                                        before: before,
                                        page: (taskData.page + 1)
                                    }, self.getProcessSinglePage());
                        }
                        finishTask();
                    });
                }
                catch (err) {
                    console.dir(err);
                    finishTask();
                }
            });
        };
    }

    return this.processSinglePage;
};

TagsModel.prototype.insert = function(input, callback) {
    this.daoFactory.getTagsDao().insertSingleDoc(input, callback);
};

/**
 * Getting documents for output
 *
 * @param {Object} input
 * @param {Function} callback
 * @returns {undefined}
 */
TagsModel.prototype.getForList = function(input, callback) {
    var self = this;

    this.daoFactory.getTagsDao().getByQuery(input.filter, [['_id', input.order]], input.skip * self.config.pagination.taglimit, self.config.pagination.taglimit,
            function(err, docs, count) {
                if (err) {
                    return callback(err, null, null, null);
                }

                callback(null, docs, (input.skip + 1) * self.config.pagination.taglimit < count, count);
            });
};

/**
 * Remove tag from collection
 *
 * @param {Object} input
 * @param {Function} callback
 * @returns {undefined}
 */
TagsModel.prototype.remove = function(input, callback) {
    this.daoFactory.getTagsDao().remove(input, callback);
};

/**
 * Main tags processing function
 *
 * @param {Function} callback
 * @returns {undefined}
 */
TagsModel.prototype.process = function(callback) {
    var self = this;
    this.daoFactory.getTagsDao().getByQueryIterative({}, null, null, null, function(doc) {
        self.taskManager.addTask({
            tag: doc["_id"],
            before: 0,
            page: 1
        }, self.getProcessSinglePage());
    }, function(err) {
        return callback(err);
    });
};

module.exports = function() {
    var o = Object.create(TagsModel.prototype);
    return TagsModel.apply(o, arguments);
};