/*
 * Config file
 * 
 */

// Example settings
module.exports = {
    url: "http://localhost:3000", // Grabber url after OAuth authorization
    mongodb_connection_line: "mongodb://localhost:27017/tbc", //MongoDB connection settings
    taskManager: {
        pause: 100, //Pause which occurs after every tick of task manager
        maxThreads: 2 //Maximum number of threads
    },
    listen_port: 3000, //What port should I listen?
    listen_ip: "127.0.0.1", //What ip should I listen?
    live_time: 18 * 60 * 60 * 1000, //For how long should process live before reload, appled only to cluster variant
    live_time_delta: 30 * 60 * 60 * 1000, //Will be added to live time to make sure that all process will not reload at same time
    tumblr_api_auth: {//Tumblr API auth
        consumer_key: "your-key-here",
        consumer_secret: "your-secret-here",
        callback: "http://localhost:3000/sessions/oauth-accept" //callback for Tumblr OAuth authorization
    },
    user_info_query: {read: 1}, //Query for getting all blog which info needs to be updated
    process_post_only_once: false, // If true, will process for links only unprocessed posts
    tagged_pages: {
        limit: 3, //1-20, limit for posts per tagged page
        pages: 5 //Look for last x pages with tagged posts
    },
    read_pages: {
        limit: 5 //1-20, limit for posts per blog page
    },
    followers_pages: {// Limit for blogs per followers and following pages
        limit: 5
    },
    availability: {
        auto: true, // Should script autoupdate availability
        duration: 6480000 //Duration in second in which blog counts as available
    },
    request: {
        timeout: 10000,
        followRedirect: true,
        maxRedirects: 5
    },
    pagination: {
        taglimit: 5, // Limit per page with tags
        blogslimit: 5, // Limit per page with blogs
        postslimit: 5, // Limit per page with posts
        autocompleteLimit: 5 // Limit for autocomplete
    },
    auth: {
        required: true, //Is authorization required
        login: "login", //User login
        password: "password", //User password
        secret: "133TSuppASEEKREET", //Secret
        name: "SID" //Cookie session name
    },
    cache: {
        time: 1800000 //Caching time in milliseconds for static
    },
    loader: {
        app: {//Settings for server
            bootstrap: false, //Bootstrap all source before starting
            map: ["./dao", "./models", "./routes"], //Which directories should be bootstrapped
            preload: false //Fully load all objects rather than load them on demand
        },
        getter: {//Settings for grabber
            bootstrap: false,
            map: ["./dao", "./models"],
            preload: false
        }
    },
    static_dir: "/public/dev/", // Directory with static files
    posts_export: {
        title: 'Tumblr Blog Crawler',
        site_url: 'http://localhost:3000',
        feed_url: 'http://localhost:3000/posts/export?type=rss',
        description: 'Tumblr Blog Crawler feed',
        author: 'OmniLRenegadE',
        limit: 10, // Limit for posts in RSS lent
        caching: 30 * 60 // Time for caching in seconds
    },
    blogs_export: {
        caching: 30 * 60 // Time for caching in seconds
    },
    import_override_permission: true // Should user authorize on tbc to import his followers and followees?
};