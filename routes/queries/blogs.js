/**
 * Input validator and transformer for blogs
 *
 * @returns {BlogQueries}
 */
var BlogQueries = function() {
    this.parent.apply(this, arguments);

    this.parser = null;

    return this;
};

BlogQueries.prototype = require("./index")();
BlogQueries.prototype.parent = BlogQueries.prototype.constructor;
BlogQueries.prototype.constructor = BlogQueries;

/**
 * Autoloader
 *
 * @returns {undefined}
 */
BlogQueries.prototype.__preload = function() {
    console.log('Preloading blogs input wrapper');

    this.getParser();
};

/**
 * Getting parser object
 *
 * @returns {Parser}
 */
BlogQueries.prototype.getParser = function() {
    this.parser = this.parser || require("./../../helpers/parser")();
    return this.parser;
};

/**
 * Parser for blog list
 *
 * @param {Object} input
 * @returns {Object}
 */
BlogQueries.prototype.showBlogList = function(input) {
    var result = {skip: 0, order: 1, filter: {}};

    result.skip = this.checkSkip(input);
    result.order = this.checkOrder(input);

    this.checkArray(input, 'process', true, function(value) {
        result.filter.process = result.filter.process || {"$in": []};

        var parsedValue = parseInt(value);
        if (!isNaN(parsedValue) && parsedValue >= -1 && parsedValue <= 1 && isFinite(parsedValue) && parsedValue.toString() === value && typeof value === 'string') {
            result.filter.process["$in"].push(parsedValue);
        }
        else
        {
            throw new Error("Invalid process value");
        }
    });

    this.checkArray(input, 'not_available', true, function(value) {
        result.filter.not_available = result.filter.not_available || {"$in": []};

        var parsedValue = parseInt(value);
        if (!isNaN(parsedValue) && parsedValue >= 0 && parsedValue <= 1 && isFinite(parsedValue) && parsedValue.toString() === value && typeof value === 'string') {
            result.filter.not_available["$in"].push(parsedValue);
        }
        else
        {
            throw new Error("Invalid not available value");
        }
    });

    this.checkArray(input, 'read', true, function(value) {
        result.filter.read = result.filter.read || {"$in": []};

        var parsedValue = parseInt(value);
        if (!isNaN(parsedValue) && parsedValue >= 0 && parsedValue <= 1 && isFinite(parsedValue) && parsedValue.toString() === value && typeof value === 'string') {
            result.filter.read["$in"].push(parsedValue);
        }
        else
        {
            throw new Error("Invalid read value");
        }
    });

    this.checkArray(input, 'export', true, function(value) {
        result.filter.export = result.filter.export || {"$in": []};

        var parsedValue = parseInt(value);
        if (!isNaN(parsedValue) && parsedValue >= 0 && parsedValue <= 1 && isFinite(parsedValue) && parsedValue.toString() === value && typeof value === 'string') {
            result.filter.export["$in"].push(parsedValue);
        }
        else
        {
            throw new Error("Invalid export value");
        }
    });


    if ((typeof input._id !== "undefined") && input._id.toString() !== "") {
        if (typeof input._id !== "string") {
            throw new Error("Invalid blog name value");
        }

        result.filter._id = {$regex: input._id, $options: 'i'};
    }

    if ((typeof input.blog_title !== "undefined") && input.blog_title.toString() !== "") {
        if (typeof input.blog_title !== "string") {
            throw new Error("Invalid blog title value");
        }

        result.filter['info.title'] = {$regex: input.blog_title, $options: 'i'};
    }

    return result;
};

/**
 * Handling common parameters of insert and update
 *
 * @param {type} input
 * @param {type} result
 * @returns {unresolved}
 */
BlogQueries.prototype.insertUpdateCommonParams = function(input, result) {
    if (typeof input.note !== "undefined") {
        if (typeof input.note !== "string") {
            throw new Error("Invalid note value");
        }
        result.params.note = input.note.toString();
    }

    var parsedValue;
    if (typeof input.process !== "undefined") {
        parsedValue = parseInt(input.process);

        if (isNaN(parsedValue) || parsedValue < -1 || parsedValue > 1 || !isFinite(parsedValue) || parsedValue.toString() !== input.process || typeof input.process !== 'string') {
            throw new Error("Invalid process value");
        }

        result.params.process = parsedValue;
    }

    if (typeof input.not_available !== "undefined") {
        parsedValue = parseInt(input.not_available);

        if (isNaN(parsedValue) || parsedValue < 0 || parsedValue > 1 || !isFinite(parsedValue) || parsedValue.toString() !== input.not_available || typeof input.not_available !== 'string') {
            throw new Error("Invalid not_available value");
        }

        result.params.not_available = parsedValue;
    }

    if (typeof input.read !== "undefined") {
        parsedValue = parseInt(input.read);

        if (isNaN(parsedValue) || parsedValue < 0 || parsedValue > 1 || !isFinite(parsedValue) || parsedValue.toString() !== input.read || typeof input.read !== 'string') {
            throw new Error("Invalid read value");
        }

        result.params.read = parsedValue;
    }

    if (typeof input.export !== "undefined") {
        parsedValue = parseInt(input.export);

        if (isNaN(parsedValue) || parsedValue < 0 || parsedValue > 1 || !isFinite(parsedValue) || parsedValue.toString() !== input.export || typeof input.export !== 'string') {
            throw new Error("Invalid export value");
        }

        result.params.export = parsedValue;
    }

    return result;
};

/**
 * Parser for insert
 *
 * @param {Object} input
 * @return {Object}
 */
BlogQueries.prototype.insert = function(input) {
    var result = {_id: "", params: {}};

    if (typeof input._id === "undefined") {
        throw new Error("Missing id value");
    } else
    if (typeof input._id !== "string") {
        throw new Error("Invalid id value");
    } else
    {
        input._id = this.getParser().prepareLink(input._id);
        if (!input._id) {
            throw new Error("Invalid id value");
        }
        else {
            result._id = input._id;
        }
    }

    result = this.insertUpdateCommonParams(input, result);

    return result;
};

/**
 * Parser for update
 *
 * @param {Object} input
 * @return {Object}
 */
BlogQueries.prototype.update = function(input) {
    var result = {_id: "", params: {}};

    if (typeof input._id === "undefined") {
        throw new Error("Missing id value");
    } else
    if (typeof input._id !== "string") {
        throw new Error("Invalid id value");
    } else
    {
        result._id = input._id.toString();
    }

    result = this.insertUpdateCommonParams(input, result);

    return result;
};

/**
 * Parser for insert and update
 *
 * @param {Object} input
 * @return {Object}
 */
BlogQueries.prototype.search = function(input) {
    if (typeof input.query !== "string") {
        throw new Error("Invalid search query type");
    }

    return input.query.toString();
};

/**
 * Parser for insert and update
 *
 * @param {Object} input
 * @return {Object}
 */
BlogQueries.prototype.import = function(input) {
    var query = {type: "", text: ""};

    if (input.type !== "parse_html" && input.type !== "parse_uri" && input.type !== "import_uri") {
        throw new Error("Invalid type value");
    }
    query.type = input.type;

    if (typeof input.text !== "string") {
        throw new Error("Invalid text value");
    }
    if (!input.text) {
        throw new Error("Text is empty");
    }
    query.text = input.text.toString();

    return query;
};

var validator = null;
module.exports = function() {
    validator = validator || new BlogQueries();
    return validator;
};