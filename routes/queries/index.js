/**
 * Parent class for queries checkers
 *
 * @returns {CommonQueries}
 */
var CommonQueries = function() {
    return this;
};

/**
 * Checking array
 *
 * @param {Object} input
 * @param {String} name
 * @param {Boolean} allowEmpty
 * @param {Function} valueChecker
 * @returns {undefined}
 */
CommonQueries.prototype.checkArray = function(input, name, allowEmpty, valueChecker) {
    if (typeof input[name] === "undefined") {
        if (!allowEmpty) {
            throw new Error(name + " value is missing");
        }
    }
    else
    {
        if (!(input[name] instanceof Array)) {
            throw new Error(name + " value must be array");
        }
        input[name].forEach(valueChecker);
    }
};

/**
 * Checking page number
 *
 * @param {Object} input
 * @returns {Object}
 */
CommonQueries.prototype.checkSkip = function(input) {
    var result = 0;

    if (typeof input.skip === "undefined") {
        result = 0;
    }
    else
    {
        result = parseInt(input.skip);
        if (isNaN(result) || result < 0 || (typeof input.skip !== 'string') || result.toString() !== input.skip || !isFinite(result)) {
            throw new Error("Page value must be integer");
        }
    }

    return result;
};

/**
 * Checking order number
 *
 * @param {Object} input
 * @returns {Object}
 */
CommonQueries.prototype.checkOrder = function(input) {
    var result = 1;

    if (typeof input.order === "undefined") {
        result = 1;
    }
    else
    {
        result = parseInt(input.order);
        if (isNaN(result) || Math.abs(result) !== 1 || (typeof input.order !== 'string') || result.toString() !== input.order) {
            throw new Error("Order must be integer");
        }
    }

    return result;
};

module.exports = function() {
    var o = Object.create(CommonQueries.prototype);
    return CommonQueries.apply(o, arguments);
};