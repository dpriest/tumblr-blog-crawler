/**
 * Input validator and transformer for tags
 *
 * @returns {TagQueries}
 */
var TagQueries = function() {
    this.parent.apply(this, arguments);

    return this;
};

TagQueries.prototype = require("./index")();
TagQueries.prototype.parent = TagQueries.prototype.constructor;
TagQueries.prototype.constructor = TagQueries;

/**
 * Parser for insert and remove
 *
 * @return {Object}
 */
TagQueries.prototype.insert = TagQueries.prototype.remove = function(input) {
    var result = {};

    if (typeof input._id === "undefined" || input._id.toString() === "") {
        throw new Error("Tag is missing");
    } else
    if (typeof input._id !== "string") {
        throw new Error("Invalid tag name value");
    }
    else
    {
        result._id = input._id.toString();
    }

    return result;
};

/**
 * Parser for tag list
 *
 * @param {Object} input
 * @returns {Object}
 */
TagQueries.prototype.showTagList = function(input) {
    var result = {skip: 0, order: 1, filter: {}};

    result.skip = this.checkSkip(input);
    result.order = this.checkOrder(input);

    if ((typeof input._id !== "undefined") && input._id.toString() !== "") {
        if (typeof input._id !== "string") {
            throw new Error("Invalid tag name value");
        }

        result.filter._id = {$regex: input._id, $options: 'i'};
    }

    return result;
};

var validator = null;
module.exports = function() {
    validator = validator || new TagQueries();
    return validator;
};