/**
 * Input validator and transformer for posts
 *
 * @returns {PostsQueries}
 */
var PostsQueries = function() {
    this.parent.apply(this, arguments);

    return this;
};

PostsQueries.prototype = require("./index")();
PostsQueries.prototype.parent = PostsQueries.prototype.constructor;
PostsQueries.prototype.constructor = PostsQueries;

/**
 * Parser for post list
 *
 * @param {Object} input
 * @returns {Object}
 */
PostsQueries.prototype.showPostList = function(input) {
    var result = {skip: 0, order: 1, filter: {}};

    result.skip = this.checkSkip(input);
    result.order = this.checkOrder(input);

    this.checkArray(input, 'blogs', true, function(value) {
        result.filter.blog = result.filter.blog || {"$in": []};

        if (typeof value !== "string") {
            throw new Error("Invalid blog value");
        }

        result.filter.blog["$in"].push(new RegExp(value.toString(), "i"));
    });

    var tagFilter = null;
    this.checkArray(input, 'tags', true, function(value) {
        tagFilter = tagFilter || [{"info.tags": {"$in": []}}, {"user_tags": {"$in": []}}];

        if (typeof value !== "string") {
            throw new Error("Invalid tag value");
        }

        tagFilter[0]["info.tags"]["$in"].push(new RegExp(value.toString(), "i"));
        tagFilter[1]["user_tags"]["$in"].push(new RegExp(value.toString(), "i"));
    });
    if (tagFilter) {
        result.filter["$or"] = tagFilter;
    }

    this.checkArray(input, 'liked', true, function(value) {
        result.filter.user_liked = result.filter.user_liked || {"$in": []};

        var parsedValue = parseInt(value);
        if (!isNaN(parsedValue) && parsedValue >= 0 && parsedValue <= 1 && isFinite(parsedValue) && parsedValue.toString() === value && typeof value === "string") {
            result.filter.user_liked["$in"].push(parsedValue ? true : false);
        }
        else
        {
            throw new Error("Invalid like value");
        }
    });

    var supportedTypes = ["text", "quote", "link", "answer", "video", "audio", "photo", "chat"];
    this.checkArray(input, 'types', true, function(value) {
        result.filter["info.type"] = result.filter["info.type"] || {"$in": []};

        if (typeof value !== "string") {
            throw new Error("Invalid type value");
        }

        value = value.toString();
        for (var i = 0; i < supportedTypes.length; i++) {
            if (supportedTypes[i] === value) {
                result.filter["info.type"]["$in"].push(value);
                break;
            }
        }

        if (i >= supportedTypes.length)
        {
            throw new Error("Invalid type value");
        }
    });

    return result;
};

/**
 * Parser for user likes
 *
 * @param {Object} input
 * @returns {Object}
 */
PostsQueries.prototype.like = function(input) {
    var result = {_id: null, user_liked: 0};

    if (typeof input._id === "undefined") {
        throw new Error("Post id undefined");
    }

    if (typeof input._id !== "string") {
        throw new Error("Post id must be integer");
    }

    result._id = Number(input._id);
    if (result._id === null || result._id.toString() !== input._id) {
        throw new Error("Post id must be integer");
    }


    if (typeof input.like === "undefined") {
        throw new Error("Post like undefined");
    }

    if (typeof input.like !== "string") {
        throw new Error("Post like has invalid value");
    }

    result.user_liked = Number(input.like);
    if (result.user_liked !== 0 && result.user_liked !== 1 || result._id.toString() !== input._id) {
        throw new Error("Post like has invalid value");
    }

    return result;
};

/**
 * Parser for user tags
 *
 * @param {Object} input
 * @returns {Object}
 */
PostsQueries.prototype.userTag = function(input) {
    var result = {_id: null, tag: ""};

    if (typeof input._id === "undefined") {
        throw new Error("Post id is undefined");
    }

    if (typeof input._id !== "string") {
        throw new Error("Post id must be integer");
    }

    result._id = Number(input._id);
    if (result._id === null || result._id.toString() !== input._id) {
        throw new Error("Post id must be integer");
    }


    if (typeof input.tag === "undefined") {
        throw new Error("Tag is undefined");
    }

    if (typeof input.tag !== "string") {
        throw new Error("Tag has invalid value");
    }

    result.tag = input.tag.toString();

    if (typeof input.add === "undefined") {
        throw new Error("Tag operation is undefined");
    }

    if (typeof input.add !== "string") {
        throw new Error("Tag operation has invalid value");
    }

    result.add = Number(input.add);
    if ((result.add !== 0 && result.add !== 1) || result.add.toString() !== input.add) {
        throw new Error("Tag operation has invalid value");
    }

    return result;
};

/**
 * Parser for export
 *
 * @param {Object} input
 * @returns {Object}
 */
PostsQueries.prototype.export = function(input) {
    var result = {type: ""};

    if (input.type !== "rss") {
        throw new Error("Export operation has invalid value");
    }
    result.type = input.type;

    return result;
};

var validator = null;
module.exports = function() {
    validator = validator || new PostsQueries();
    return validator;
};