/**
 * Input validator and transformer for sessions
 *
 * @returns {SessionsQueries}
 */
var SessionsQueries = function() {
    this.parent.apply(this, arguments);

    return this;
};

SessionsQueries.prototype = require("./index")();
SessionsQueries.prototype.parent = SessionsQueries.prototype.constructor;
SessionsQueries.prototype.constructor = SessionsQueries;

/**
 * Parser for login
 *
 * @return {Object}
 */
SessionsQueries.prototype.login = function(input) {
    var result = {};

    if (typeof input.login === "undefined") {
        throw new Error("Login is missing");
    } else
    if (typeof input.login !== "string") {
        throw new Error("Invalid login value");
    } else
    {
        result.login = input.login.toString();
    }

    if (typeof input.password === "undefined") {
        throw new Error("Password is missing");
    } else
    if (typeof input.password !== "string") {
        throw new Error("Invalid password value");
    } else
    {
        result.password = input.password.toString();
    }

    return result;
};

var validator = null;
module.exports = function() {
    validator = validator || new SessionsQueries();
    return validator;
};