/**
 * Blog routes handler
 *
 * @param {type} daoFactory
 * @param {type} taskManager
 * @param {Object} config
 * @returns {BlogsHandler}
 */
var BlogsHandler = function(daoFactory, taskManager, config) {
    this.daoFactory = daoFactory;
    this.taskManager = taskManager;
    this.config = config;

    this.blogsModel = null;
    this.wrapper = null;
    this.inputTransformer = null;

    this.insert = null;
    this.update = null;
    this.showBlogList = null;
    this.process = null;
    this.processPosts = null;
    this.followers = null;
    this.followings = null;
    this.getInfo = null;
    this.getForExport = null;
    this.import = null;

    this.search = null;
    return this;
};

/**
 * Autoloader
 *
 * @returns {undefined}
 */
BlogsHandler.prototype.__preload = function() {
    console.log('Preloading blogs handler');

    this.getBlogsModel();
    this.getWrapper();
    this.getInputTransformer();

    this.getInsert();
    this.getUpdate();
    this.getShowBlogList();
    this.getProcess();
    this.getProcessPosts();
    this.getFollowers();
    this.getFollowings();
    this.getGetInfo();
    this.getGetForExport();
    this.getImport();
};

/**
 * Getting an output query wrapper
 *
 * @returns {QueryWrapper}
 */
BlogsHandler.prototype.getWrapper = function() {
    this.wrapper = this.wrapper || require("./queries/queryWrapper")();
    return this.wrapper;
};

/**
 * Getting blog queries transformer
 *
 * @returns {BlogQueries}
 */
BlogsHandler.prototype.getInputTransformer = function() {
    this.inputTransformer = this.inputTransformer || require("./queries/blogs")();
    return this.inputTransformer;
};

/**
 * Getting model
 *
 * @returns {BlogsModel}
 */
BlogsHandler.prototype.getBlogsModel = function() {
    this.blogsModel = this.blogsModel || require("./../models/blogs")(this.daoFactory, this.taskManager, this.config);
    return this.blogsModel;
};

/**
 * Getting insert handler
 *
 * @returns {Function}
 */
BlogsHandler.prototype.getInsert = function() {
    if (this.insert === null) {
        var self = this;

        this.insert = function(req, res, next) {
            try {
                var input = self.getInputTransformer().insert(req.body);
                self.getBlogsModel().insert(input, function(err, doc) {
                    if (err) {
                        console.dir(err);
                        res.status(200).send(self.getWrapper().wrap(false, null, err));
                    }
                    else
                    {
                        var payload = {items: [doc]};
                        res.status(200).send(self.getWrapper().wrap(true, payload, null));
                    }
                });
            }
            catch (err) {
                console.dir(err);
                res.status(200).send(self.getWrapper().wrap(false, null, err));
            }
        };
    }
    return this.insert;
};

/**
 * Getting update handler
 *
 * @returns {Function}
 */
BlogsHandler.prototype.getUpdate = function() {
    if (this.update === null) {
        var self = this;

        this.update = function(req, res, next) {
            try {
                var input = self.getInputTransformer().insert(req.body);
                self.getBlogsModel().update(input, function(err, docs) {
                    if (err) {
                        console.dir(err);
                        res.status(200).send(self.getWrapper().wrap(false, null, err));
                    }
                    else
                    {
                        var payload = {items: docs};
                        res.status(200).send(self.getWrapper().wrap(true, payload, null));
                    }
                });
            }
            catch (err) {
                console.dir(err);
                res.status(200).send(self.getWrapper().wrap(false, null, err));
            }
        };
    }
    return this.update;
};

/**
 * Getting blog list handler
 *
 * @returns {Function}
 */
BlogsHandler.prototype.getShowBlogList = function() {
    if (this.showBlogList === null) {
        var self = this;

        this.showBlogList = function(req, res, next) {
            try {
                var input = self.getInputTransformer().showBlogList(req.body);
                self.getBlogsModel().getForList(input, function(err, docs, hasNext, count) {
                    if (err) {
                        console.dir(err);
                        res.status(200).send(self.getWrapper().wrap(false, null, err));
                    }
                    else
                    {
                        var payload = {items: docs, hasNext: hasNext, count: count};
                        res.status(200).send(self.getWrapper().wrap(true, payload, null));
                    }
                });
            }
            catch (err) {
                console.dir(err);
                res.status(200).send(self.getWrapper().wrap(false, null, err));
            }
        };
    }
    return this.showBlogList;
};

/**
 * Getting process handler
 *
 * @returns {Function}
 */
BlogsHandler.prototype.getProcess = function() {
    if (this.process === null) {
        var self = this;

        this.process = function(req, res, next) {
            try {
                self.getBlogsModel().process(function(err) {
                    if (err) {
                        console.dir(err);
                        res.status(200).send(self.getWrapper().wrap(false, null, err));
                    }
                    else
                    {
                        res.status(200).send(self.getWrapper().wrap(true, null, null));
                    }
                });
            }
            catch (err) {
                console.dir(err);
                res.status(200).send(self.getWrapper().wrap(false, null, err));
            }
        };
    }
    return this.process;
};

/**
 * Getting blog info handler
 *
 * @returns {Function}
 */
BlogsHandler.prototype.getGetInfo = function() {
    if (this.getInfo === null) {
        var self = this;

        this.getInfo = function(req, res, next) {
            try {
                self.getBlogsModel().getInfo(function(err) {
                    if (err) {
                        console.dir(err);
                        res.status(200).send(self.getWrapper().wrap(false, null, err));
                    }
                    else
                    {
                        res.status(200).send(self.getWrapper().wrap(true, null, null));
                    }
                });
            }
            catch (err) {
                console.dir(err);
                res.status(200).send(self.getWrapper().wrap(false, null, err));
            }
        };
    }
    return this.getInfo;
};

/**
 * Getting export handler
 *
 * @returns {Function}
 */
BlogsHandler.prototype.getGetForExport = function() {
    if (this.getForExport === null) {
        var self = this;

        this.getForExport = function(req, res, next) {
            try {
                self.getBlogsModel().getForExport(function(err, docs) {
                    if (err) {
                        console.dir(err);
                        res.status(200).send(self.getWrapper().wrap(false, null, err));
                    }
                    else
                    {
                        var payload = {items: docs};
                        res.status(200).send(self.getWrapper().wrap(true, payload, null));
                    }
                });
            }
            catch (err) {
                console.dir(err);
                res.status(200).send(self.getWrapper().wrap(false, null, err));
            }
        };
    }
    return this.getForExport;
};

/**
 * Getting search handler
 *
 * @returns {Function}
 */
BlogsHandler.prototype.getSearch = function() {
    if (this.search === null) {
        var self = this;
        this.search = function(req, res, next) {
            try {
                var input = self.getInputTransformer().search(req.query);
                self.getBlogsModel().search(input, function(err, docs) {
                    if (err) {
                        console.dir(err);
                        res.status(200).send(self.getWrapper().wrap(false, null, err));
                    }
                    else
                    {
                        res.status(200).send(self.getWrapper().wrap(true, docs, null));
                    }
                });
            }
            catch (err) {
                console.dir(err);
                res.status(200).send(self.getWrapper().wrap(false, null, err));
            }
        };
    }
    return this.search;
};

/**
 * Getting import handler
 *
 * @returns {Function}
 */
BlogsHandler.prototype.getImport = function() {
    if (this.import === null) {
        var self = this;

        this.import = function(req, res, next) {
            try {
                var input = self.getInputTransformer().import(req.body);
                self.getBlogsModel().import(input, function(err) {
                    if (err) {
                        console.dir(err);
                        res.status(200).send(self.getWrapper().wrap(false, null, err));
                    }
                    else
                    {
                        res.status(200).send(self.getWrapper().wrap(true, null, null));
                    }
                });
            }
            catch (err) {
                console.dir(err);
                res.status(200).send(self.getWrapper().wrap(false, null, err));
            }
        };
    }
    return this.import;
};

/**
 * Getting import followers handler
 *
 * @returns {Function}
 */
BlogsHandler.prototype.getFollowers = function() {
    if (this.followers === null) {
        var self = this;

        this.followers = function(req, res, next) {
            try {
                if (!req.session.oauth_access) {
                    return res.status(200).send(self.getWrapper().wrap(false, null, "Authorize with tumblr"));
                }

                self.getBlogsModel().importFollowers(req.session.oauth_access.access_token, req.session.oauth_access.access_secret, function(err) {
                    if (err) {
                        console.dir(err);
                        res.status(200).send(self.getWrapper().wrap(false, null, err));
                    }
                    else
                    {
                        res.status(200).send(self.getWrapper().wrap(true, null, null));
                    }
                });
            }
            catch (err) {
                console.dir(err);
                res.status(200).send(self.getWrapper().wrap(false, null, err));
            }
        };
    }
    return this.followers;
};

/**
 * Getting import following blogs handler
 *
 * @returns {Function}
 */
BlogsHandler.prototype.getFollowings = function() {
    if (this.followings === null) {
        var self = this;

        this.followings = function(req, res, next) {
            try {
                if (!req.session.oauth_access) {
                    return res.status(200).send(self.getWrapper().wrap(false, null, "Authorize with tumblr"));
                }

                self.getBlogsModel().importFollowings(req.session.oauth_access.access_token, req.session.oauth_access.access_secret, function(err) {
                    if (err) {
                        console.dir(err);
                        res.status(200).send(self.getWrapper().wrap(false, null, err));
                    }
                    else
                    {
                        res.status(200).send(self.getWrapper().wrap(true, null, null));
                    }
                });
            }
            catch (err) {
                console.dir(err);
                res.status(200).send(self.getWrapper().wrap(false, null, err));
            }
        };
    }
    return this.followings;
};

/**
 * Getting process posts handler
 *
 * @returns {Function}
 */
BlogsHandler.prototype.getProcessPosts = function() {
    if (this.processPosts === null) {
        var self = this;

        this.processPosts = function(req, res, next) {
            try {
                self.getBlogsModel().processPosts(function(err) {
                    if (err) {
                        console.dir(err);
                        res.status(200).send(self.getWrapper().wrap(false, null, err));
                    }
                    else
                    {
                        res.status(200).send(self.getWrapper().wrap(true, null, null));
                    }
                });
            }
            catch (err) {
                console.dir(err);
                res.status(200).send(self.getWrapper().wrap(false, null, err));
            }
        };
    }
    return this.processPosts;
};

module.exports = function() {
    var o = Object.create(BlogsHandler.prototype);
    return BlogsHandler.apply(o, arguments);
};