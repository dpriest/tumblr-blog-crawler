/**
 * Tags routes handler
 *
 * @param {type} daoFactory
 * @param {type} taskManager
 * @param {Object} config
 * @returns {TagsHandler}
 */
var TagsHandler = function(daoFactory, taskManager, config) {
    this.daoFactory = daoFactory;
    this.taskManager = taskManager;
    this.config = config;

    this.tagsModel = null;
    this.wrapper = null;
    this.inputTransformer = null;

    this.insert = null;
    this.remove = null;
    this.showTagList = null;
    this.process = null;

    return this;
};

/**
 * Autoloader
 *
 * @returns {undefined}
 */
TagsHandler.prototype.__preload = function() {
    console.log('Preloading tags handler');

    this.getTagsModel();
    this.getWrapper();
    this.getInputTransformer();

    this.getInsert();
    this.getRemove();
    this.getShowTagList();
    this.getProcess();
};

/**
 * Getting an output query wrapper
 *
 * @returns {QueryWrapper}
 */
TagsHandler.prototype.getWrapper = function() {
    this.wrapper = this.wrapper || require("./queries/queryWrapper")();
    return this.wrapper;
};

/**
 * Getting tag queries transformer
 *
 * @returns {TagQueries}
 */
TagsHandler.prototype.getInputTransformer = function() {
    this.inputTransformer = this.inputTransformer || require("./queries/tags")();
    return this.inputTransformer;
};

/**
 * Getting model
 *
 * @returns {TagsModel}
 */
TagsHandler.prototype.getTagsModel = function() {
    this.tagsModel = this.tagsModel || require("./../models/tags")(this.daoFactory, this.taskManager, this.config);
    return this.tagsModel;
};

/**
 * Getting insert handler
 *
 * @returns {Function}
 */
TagsHandler.prototype.getInsert = function() {
    if (this.insert === null) {
        var self = this;

        this.insert = function(req, res, next) {
            try {
                var input = self.getInputTransformer().insert(req.body);
                self.getTagsModel().insert(input, function(err, doc) {
                    if (err) {
                        console.dir(err);
                        res.status(200).send(self.getWrapper().wrap(false, null, err));
                    }
                    else
                    {
                        var payload = {items: [doc]};
                        res.status(200).send(self.getWrapper().wrap(true, payload, null));
                    }
                });
            }
            catch (err) {
                console.dir(err);
                res.status(200).send(self.getWrapper().wrap(false, null, err));
            }
        };
    }
    return this.insert;
};

/**
 * Getting tag list handler
 *
 * @returns {Function}
 */
TagsHandler.prototype.getShowTagList = function() {
    if (this.showTagList === null) {
        var self = this;

        this.showTagList = function(req, res, next) {
            try {
                var input = self.getInputTransformer().showTagList(req.body);
                self.getTagsModel().getForList(input, function(err, docs, hasNext, count) {
                    if (err) {
                        console.dir(err);
                        res.status(200).send(self.getWrapper().wrap(false, null, err));
                    }
                    else
                    {
                        var payload = {items: docs, hasNext: hasNext, count: count};
                        res.status(200).send(self.getWrapper().wrap(true, payload, null));
                    }
                });
            }
            catch (err) {
                console.dir(err);
                res.status(200).send(self.getWrapper().wrap(false, null, err));
            }
        };
    }
    return this.showTagList;
};

/**
 * Getting remove handler
 *
 * @returns {Function}
 */
TagsHandler.prototype.getRemove = function() {
    if (this.remove === null) {
        var self = this;

        this.remove = function(req, res, next) {
            try {
                var input = self.getInputTransformer().remove(req.body);
                self.getTagsModel().remove(input, function(err) {
                    if (err) {
                        console.dir(err);
                        res.status(200).send(self.getWrapper().wrap(false, null, err));
                    }
                    else
                    {
                        res.status(200).send(self.getWrapper().wrap(true, null, null));
                    }
                });
            }
            catch (err) {
                console.dir(err);
                res.status(200).send(self.getWrapper().wrap(false, null, err));
            }
        };
    }
    return this.remove;
};

/**
 * Getting process handler
 *
 * @returns {Function}
 */
TagsHandler.prototype.getProcess = function() {
    if (this.process === null) {
        var self = this;

        this.process = function(req, res, next) {
            try {
                self.getTagsModel().process(function(err) {
                    if (err) {
                        console.dir(err);
                        res.status(200).send(self.getWrapper().wrap(false, null, err));
                    }
                    else
                    {
                        res.status(200).send(self.getWrapper().wrap(true, null, null));
                    }
                });
            }
            catch (err) {
                console.dir(err);
                res.status(200).send(self.getWrapper().wrap(false, null, err));
            }
        };
    }
    return this.process;
};

module.exports = function() {
    var o = Object.create(TagsHandler.prototype);
    return TagsHandler.apply(o, arguments);
};