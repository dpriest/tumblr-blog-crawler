/**
 * Posts routes handler
 *
 * @param {type} daoFactory
 * @param {type} taskManager
 * @param {Object} config
 * @returns {PostsHandler}
 */
var PostsHandler = function(daoFactory, taskManager, config) {
    this.daoFactory = daoFactory;
    this.taskManager = taskManager;
    this.config = config;

    this.postsModel = null;
    this.wrapper = null;
    this.inputTransformer = null;

    this.process = null;
    this.showPostList = null;
    this.like = null;
    this.userTag = null;
    this.export = null;
    this.update = null;

    return this;
};

/**
 * Autoloader
 *
 * @returns {undefined}
 */
PostsHandler.prototype.__preload = function() {
    console.log('Preloading posts handler');

    this.getPostsModel();
    this.getWrapper();
    this.getInputTransformer();

    this.getProcess();
    this.getShowPostList();
    this.getLike();
    this.getUserTag();
    this.getExport();
};

/**
 * Getting an output query wrapper
 *
 * @returns {QueryWrapper}
 */
PostsHandler.prototype.getWrapper = function() {
    this.wrapper = this.wrapper || require("./queries/queryWrapper")();
    return this.wrapper;
};

/**
 * Getting sessions queries transformer
 *
 * @returns {SessionsQueries}
 */
PostsHandler.prototype.getInputTransformer = function() {
    this.inputTransformer = this.inputTransformer || require("./queries/posts")();
    return this.inputTransformer;
};

/**
 * Getting model
 *
 * @returns {PostsModel}
 */
PostsHandler.prototype.getPostsModel = function() {
    this.postsModel = this.postsModel || require("./../models/posts")(this.daoFactory, this.taskManager, this.config);
    return this.postsModel;
};

/**
 * Getting process handler
 *
 * @returns {Function}
 */
PostsHandler.prototype.getProcess = function() {
    if (this.process === null) {
        var self = this;

        this.process = function(req, res, next) {
            try {
                self.getPostsModel().startRead(function(err) {
                    if (err) {
                        console.dir(err);
                        res.status(200).send(self.getWrapper().wrap(false, null, err));
                    }
                    else
                    {
                        res.status(200).send(self.getWrapper().wrap(true, null, null));
                    }
                });
            }
            catch (err) {
                console.dir(err);
                res.status(200).send(self.getWrapper().wrap(false, null, err));
            }
        };
    }
    return this.process;
};

/**
 * Getting post list handler
 *
 * @returns {Function}
 */
PostsHandler.prototype.getShowPostList = function() {
    if (this.showPostList === null) {
        var self = this;

        this.showPostList = function(req, res, next) {
            try {
                var input = self.getInputTransformer().showPostList(req.body);
                self.getPostsModel().getForList(input, function(err, docs, hasNext, count) {
                    if (err) {
                        console.dir(err);
                        res.status(200).send(self.getWrapper().wrap(false, null, err));
                    }
                    else
                    {
                        var payload = {items: docs, hasNext: hasNext, count: count};
                        res.status(200).send(self.getWrapper().wrap(true, payload, null));
                    }
                });
            }
            catch (err) {
                console.dir(err);
                res.status(200).send(self.getWrapper().wrap(false, null, err));
            }
        };
    }
    return this.showPostList;
};

/**
 * Getting post like handler
 *
 * @returns {Function}
 */
PostsHandler.prototype.getLike = function() {
    if (this.like === null) {
        var self = this;

        this.like = function(req, res, next) {
            try {
                var input = self.getInputTransformer().like(req.query);
                self.getPostsModel().like(input, function(err) {
                    if (err) {
                        console.dir(err);
                        res.status(200).send(self.getWrapper().wrap(false, null, err));
                    }
                    else
                    {
                        res.status(200).send(self.getWrapper().wrap(true, {like: input.user_liked}, null));
                    }
                });
            }
            catch (err) {
                console.dir(err);
                res.status(200).send(self.getWrapper().wrap(false, null, err));
            }
        };
    }
    return this.like;
};

/**
 * Getting post tag handler
 *
 * @returns {Function}
 */
PostsHandler.prototype.getUserTag = function() {
    if (this.userTag === null) {
        var self = this;

        this.userTag = function(req, res, next) {
            try {
                var input = self.getInputTransformer().userTag(req.query);
                self.getPostsModel().userTag(input, function(err) {
                    if (err) {
                        console.dir(err);
                        res.status(200).send(self.getWrapper().wrap(false, null, err));
                    }
                    else
                    {
                        res.status(200).send(self.getWrapper().wrap(true, {_id: input._id, tag: input.tag, add: input.add}, null));
                    }
                });
            }
            catch (err) {
                console.dir(err);
                res.status(200).send(self.getWrapper().wrap(false, null, err));
            }
        };
    }
    return this.userTag;
};

/**
 * Getting export handler
 *
 * @returns {Function}
 */
PostsHandler.prototype.getExport = function() {
    if (this.export === null) {
        var self = this;

        this.export = function(req, res, next) {
            try {
                var input = self.getInputTransformer().export(req.query);
                self.getPostsModel().export(function(err, render) {
                    if (err) {
                        console.dir(err);
                        res.status(200).send(self.getWrapper().wrap(false, null, err));

                    }
                    else
                    {
                        try {
                            res.status(200).send(render());
                        }
                        catch (err) {
                            console.dir(err);
                            res.status(200).send(self.getWrapper().wrap(false, null, err));
                        }
                    }
                });
            }
            catch (err) {
                console.dir(err);
                res.status(200).send(self.getWrapper().wrap(false, null, err));
            }
        };
    }
    return this.export;
};

/**
 * Getting post updating handler
 *
 * @returns {Function}
 */
PostsHandler.prototype.getUpdate = function() {
    if (this.update === null) {
        var self = this;

        this.update = function(req, res, next) {
            try {
                self.getPostsModel().startUpdatePosts(function(err) {
                    if (err) {
                        console.dir(err);
                        res.status(200).send(self.getWrapper().wrap(false, null, err));
                    }
                    else
                    {
                        res.status(200).send(self.getWrapper().wrap(true, null, null));
                    }
                });
            }
            catch (err) {
                console.dir(err);
                res.status(200).send(self.getWrapper().wrap(false, null, err));
            }
        };
    }
    return this.update;
};


module.exports = function() {
    var o = Object.create(PostsHandler.prototype);
    return PostsHandler.apply(o, arguments);
};