/**
 * Utils routes handler
 *
 * @param {Object} config
 * @returns {UtilsHandler}
 */
var UtilsHandler = function(taskManager, config) {
    this.taskManager = taskManager;
    this.config = config;

    this.healthModel = null;
    this.wrapper = null;

    this.health = null;

    return this;
};

/**
 * Autoloader
 *
 * @returns {undefined}
 */
UtilsHandler.prototype.__preload = function() {
    console.log('Preloading utils handler');

    this.getHealthModel();
    this.getWrapper();

    this.getHealth();
};

/**
 * Getting an output query wrapper
 *
 * @returns {QueryWrapper}
 */
UtilsHandler.prototype.getWrapper = function() {
    this.wrapper = this.wrapper || require("./queries/queryWrapper")();
    return this.wrapper;
};

/**
 * Getting model
 *
 * @returns {TagsModel}
 */
UtilsHandler.prototype.getHealthModel = function() {
    this.healthModel = this.healthModel || require("./../models/health")(this.taskManager, this.config);
    return this.healthModel;
};

/**
 * Getting process handler
 *
 * @returns {Function}
 */
UtilsHandler.prototype.getHealth = function() {
    if (this.health === null) {
        var self = this;

        this.health = function(req, res, next) {
            try {
                self.getHealthModel().getHealth(function(err, data) {
                    if (err) {
                        console.dir(err);
                        res.status(200).send(self.getWrapper().wrap(false, null, err));
                    }
                    else
                    {
                        res.status(200).send(self.getWrapper().wrap(true, data, null));
                    }
                });
            }
            catch (err) {
                console.dir(err);
                res.status(200).send(self.getWrapper().wrap(false, null, err));
            }
        };
    }
    return this.health;
};

module.exports = function() {
    var o = Object.create(UtilsHandler.prototype);
    return UtilsHandler.apply(o, arguments);
};