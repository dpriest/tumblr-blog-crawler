var cluster = require('cluster');

// The one and only master
if (cluster.isMaster) {
    console.log("Master app has been started");

    var cpuCount = 1; //require('os').cpus().length;

    cluster.on('exit', function(worker, code, signal) {
        console.log('worker %d died (%s). restarting...', worker.process.pid, signal || code);
        cluster.fork();
    });

    for (var i = 0; i < cpuCount; i++) {
        cluster.fork();
    }
}
else {
    console.log("Slave app " + cluster.worker.id + " has been started");

    require("./slave_app").startApp(true);
}