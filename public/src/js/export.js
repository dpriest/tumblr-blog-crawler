/*
 * Export
 */

/*jshint unused:false */
var launchExport = function() {
    /*jshint unused:true */

    $("#exportLinkBt, #exportLinkBtSm").addClass("active");

    $.ajax("/blogs/export", {
        dataType: "json",
        type: "GET"
    }).done(function(data) {
        $(".export-content tbody").html(swig.run(exportListTemplate, data.payload));
    }).fail(function() {
        showError("Export error:", "Server error");
    });

};