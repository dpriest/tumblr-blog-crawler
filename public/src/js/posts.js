/*
 * Posts
 */

/*jshint unused:false */
var launchPosts = function() {
    /*jshint unused:true */


    $("#postsLinkBt, #postsLinkBtSm").addClass("active");

    var getWindowContentWidth = function() {
        var windowWidth = $(window).width();

        if (windowWidth >= 1200) {
            return 603;
        }

        if (windowWidth >= 992) {
            return 565;
        }

        if (windowWidth >= 768) {
            return 538;
        }

        return windowWidth - 92;
    };
    var windowContentWidth = getWindowContentWidth();

    $(window).resize(function() {
        windowContentWidth = getWindowContentWidth();
    });

    var getAttribute = function(html, expr) {
        var start = html.search(expr);
        if (start === -1) {
            return false;
        }

        var end = start;

        while (end < html.length && (html[end] < '0' || html[end] > '9')) {
            end++;
        }

        while (end < html.length && html[end] >= '0' && html[end] <= '9') {
            end++;
        }

        if (end < html.length && html[end] === '%') {
            return false;
        }

        return html.substring(start, end + 1);
    };

    var resizeContent = function(html, contentWidth, toMax) {
        var widthAttr = getAttribute(html, /width/i);
        if (widthAttr === false) {
            return false;
        }
        var width = widthAttr.replace(/\D/g, "");

        var heightAttr = getAttribute(html, /height/i);
        if (heightAttr === false) {
            return false;
        }
        var height = heightAttr.replace(/\D/g, "");

        if (!toMax && parseFloat(width) <= contentWidth) {
            return false;
        }

        var newWidth = contentWidth + 0.0;
        var newHeight = Math.round(parseFloat(height) * (newWidth / parseFloat(width)));

        console.log(widthAttr.replace(width, newWidth), heightAttr.replace(height, newHeight));
        return html.replace(widthAttr, widthAttr.replace(width, newWidth)).replace(heightAttr, heightAttr.replace(height, newHeight));
    };

    /*
     * Rendering blog
     * 
     */

    var renderText = function(item) {
        return swig.run(postListText, {item: item});
    };

    var renderQuote = function(item) {
        return swig.run(postListQuote, {item: item});
    };

    var renderVideo = function(item) {
        /*jshint camelcase:false */
        item.player = item.info.player[0].embed_code;
        var resizedPlayer = resizeContent(item.player || "", windowContentWidth, true);
        if (resizedPlayer) {
            item.player = resizedPlayer;
        }

        /*jshint camelcase:true */
        return swig.run(postListVideo, {item: item});
    };

    var renderAudio = function(item) {
        /*jshint camelcase:false */
        var contentWidth = windowContentWidth;
        if (item.info.album_art) {
            contentWidth -= 155;
        }

        var resizedPlayer = resizeContent(item.info.player || "", contentWidth, false);
        if (resizedPlayer) {
            item.info.player = resizedPlayer;
        }

        /*jshint camelcase:true */
        return swig.run(postListAudio, {item: item});
    };

    var renderLink = function(item) {
        if (!item.info.title) {
            item.info.title = item.info.url.toString().replace(/^http:\/\//i, "").replace(/^https:\/\//i, "").replace(/^www\./, "").replace(/\/.*$/, "").replace(/\s/, "");
        }

        return swig.run(postListLink, {item: item});
    };

    var renderChat = function(item) {
        return swig.run(postListChat, {item: item});
    };

    var renderAnswer = function(item) {
        return swig.run(postListAnswer, {item: item});
    };

    /*
     * Photo subblock
     */

    /*
     * 
     * @param {type} cols
     * @returns {Number|jQuery} if <0 - it's maximum width for photo, if >0 - minimum
     */
    var getPhotoWidth = function(cols) {
        if ($(window).width() < 768) {
            return -(windowContentWidth - (cols - 1) * 6) / cols;
        }

        if (cols > 0 && cols <= 4) {
            return (700 - (cols - 1) * 6) / cols;
        }
        else
        {
            throw Error("Incorrect cols number");
        }
    };

    var processAltPhoto = function(photo, width, altPhoto) {
        if (width >= 0) {
            if (altPhoto.width < photo.thumbnail.width && altPhoto.width >= width) {
                photo.thumbnail = altPhoto;
            }
        }
        else {
            if (altPhoto.width < photo.thumbnail.width && photo.thumbnail.width > -width || altPhoto.width > photo.thumbnail.width && altPhoto.width <= -width) {
                photo.thumbnail = altPhoto;
            }
        }
    };

    var renderPhoto = function(item) {
        /*jshint camelcase:false */
        var rowLength = 1;
        if (item.info.photos.length > 2) {
            rowLength = 2;
        }

        if ($(window).width() >= 768) {
            if (item.info.photos.length > 4) {
                rowLength = 3;
            }
            if (item.info.photos.length > 6) {
                rowLength = 4;
            }
        }

        var left = item.info.photos.length;
        var layout = [];

        if (left % rowLength !== 0) {
            layout.push({length: left % rowLength});
            left -= left % rowLength;
        }

        while (left > 0) {
            left -= rowLength;
            layout.push({length: rowLength});
        }

        for (var pos = 0; pos < layout.length - 1; pos++) {
            while (layout[pos].length + 1 < layout[pos + 1].length) {
                layout[pos].length++;
                layout[pos + 1].length--;
            }
        }

        pos = 0;
        layout.forEach(function(layoutItem) {
            layoutItem.photos = [];

            for (var layoutPos = 0; layoutPos < layoutItem.length; layoutPos++) {
                var photo = {
                    original: item.info.photos[pos].original_size,
                    thumbnail: item.info.photos[pos].original_size,
                    title: item.info.photos[pos].caption,
                    space: layoutPos > 0
                };
                var width = getPhotoWidth(layoutItem.length);

                item.info.photos[pos].alt_sizes.forEach(processAltPhoto.bind(null, photo, width));
                layoutItem.photos.push(photo);
                pos++;
            }
        });
        item.layout = layout;

        return swig.run(postListPhoto, {item: item});
        /*jshint camelcase:true */
    };

    /*
     * End of photo subblock
     */

    var renderStrategy = function(item) {
        switch (item.info.type) {
            case "text":
                return renderText(item);
            case "quote":
                return renderQuote(item);
            case "video":
                return renderVideo(item);
            case "audio":
                return renderAudio(item);
            case "link":
                return renderLink(item);
            case "chat":
                return renderChat(item);
            case "answer":
                return renderAnswer(item);
            case "photo":
                return renderPhoto(item);
            default:
                return JSON.stringify(item);
        }
    };

    var renderContent = function(data) {
        data.payload.items.forEach(function(item) {
            if (item.info) {
                var date = new Date(item.info.timestamp * 1000);
                item.info.date = moment(date).format("YYYY.MM.DD HH:mm:ss");
            }
        });

        return swig.run(postListTemplate, {
            items: data.payload.items,
            userTags: function(locals) {
                return swig.run(postListUserTag, locals);
            },
            renderContent: renderStrategy
        });
    };

    /*
     * End of rendering blog
     */

    /*
     * Pagination block         
     */

    /*jshint camelcase:false */
    var defaultViewOptions = {
        blogs: [],
        tags: [],
        user_liked: [],
        types: [],
        order: -1,
        skip: 0
    };
    /*jshint camelcase:true */

    var parseArray = function(array, checkValue) {
        if (!(array instanceof Array)) {
            return false;
        }

        var correctVar = true;
        array.forEach(function(v) {
            if (!checkValue(v)) {
                correctVar = false;
            }
        });
        return correctVar;
    };
    var checkString = function(val) {
        return val.toString() !== "";
    };
    var checkYesNoValue = function(val) {
        val = Number(val);
        return (val === 0 || val === 1);
    };
    var checkType = function(val) {
        return val === "text" || val === "quote" || val === "link" || val === "answer" || val === "video" || val === "audio" || val === "photo" || val === "chat";
    };

    var parseViewOptions = function(options) {
        if (!options)
        {
            return false;
        }

        for (var property in options) {
            if (!options.hasOwnProperty(property)) {
                continue;
            }

            switch (property) {
                case "blogs":
                case "tags":
                    if (!parseArray(options[property], checkString)) {
                        return false;
                    }
                    break;
                case "user_liked":
                    if (!parseArray(options[property], checkYesNoValue)) {
                        return false;
                    }
                    break;
                case "types":
                    if (!parseArray(options[property], checkType)) {
                        return false;
                    }
                    break;
                case "order":
                    options[property] = Number(options[property]);
                    if (Math.abs(options[property]) !== 1) {
                        return false;
                    }
                    break;
                case "skip":
                    options[property] = Number(options[property]);
                    if (options[property] < 0) {
                        return false;
                    }
                    break;
                default:
                    console.log("Unknow property", property, "in blogs view options, using default");
                    return false;
            }
        }

        return true;
    };

    var saveViewOptions = function(options) {
        localStorage.setItem("postViewSettings", JSON.stringify(options));
    };

    var wrapViewOptions = function(options) {
        /*jshint camelcase:false */
        var query = {
            order: options.order,
            skip: options.skip
        };

        if (options.blogs) {
            query.blogs = options.blogs;
        }

        if (options.tags) {
            query.tags = options.tags;
        }

        if (options.user_liked) {
            query.liked = options.user_liked;
        }

        if (options.types) {
            query.types = options.types;
        }

        return query;
        /*jshint camelcase:true */
    };

    var postsContent = $(".posts-content");

    var postPagination = new Paginate(defaultViewOptions, parseViewOptions, saveViewOptions, wrapViewOptions, $(".prev-page"), $(".count-bt"), $(".next-page"), $("#refreshBt"), "/posts/show", postsContent, renderContent);
    postPagination.initOptions(JSON.parse(localStorage.getItem("postViewSettings")));
    postPagination.loadPage(postPagination.viewOptions.skip);

    /*
     * End of pagination block
     */

    /*
     * View options block
     */

    var viewOptionsBt = $("#viewOptionsBt");
    var viewOptionsModal = $("#viewOptionsModal");
    var viewOptionsForm = viewOptionsModal.find("#viewOptions");
    var viewOptionsSubmitBt = viewOptionsModal.find("#commit");

    var viewOptionsBlogs = viewOptionsForm.find(".blogs");
    var viewOptionsBlogName = viewOptionsForm.find("[name='blogname']");
    var viewOptionsTags = viewOptionsForm.find(".tags");
    var viewOptionsTagName = viewOptionsForm.find("[name='tagname']");

    var viewOptionsUserLiked = viewOptionsForm.find("input[name='user_liked']");
    var viewOptionsTypes = viewOptionsForm.find("input[name='type']");

    var viewOptionsOrder = viewOptionsForm.find("select[name='order']");
    var viewOptionsSkip = viewOptionsForm.find("input[name='skip']");

    viewOptionsBlogs.on("click", ".blog", function() {
        $(this).remove();
        return false;
    });

    viewOptionsBlogName.typeahead({
        items: 'all',
        minlength: 1,
        source: function(query, cb) {
            $.ajax("/blogs/search", {
                dataType: "json",
                type: "GET",
                data: {
                    query: query,
                }
            }).done(function(data) {
                if (!data.success) {
                    console.dir(data.error);
                    showError("Autocomplete error:", JSON.stringify(data.error));
                    cb([]);
                }
                else {
                    cb(data.payload);
                }
            }).fail(function() {
                showError("Autocomplete error:", "Server error");
                cb([]);
            });
        }
    });

    viewOptionsForm.on("click", "button[name='addblog']", function() {
        if (viewOptionsBlogName.val()) {
            viewOptionsBlogs.append(swig.run(viewOptionsPostsBlogTemplate, {list: [viewOptionsBlogName.val()]}));
        }
        return false;
    });

    viewOptionsTags.on("click", ".tag", function() {
        $(this).remove();
        return false;
    });

    viewOptionsForm.on("click", "button[name='addtag']", function() {
        if (viewOptionsTagName.val()) {
            viewOptionsTags.append(swig.run(viewOptionsPostsTagTemplate, {list: [viewOptionsTagName.val()]}));
        }
        return false;
    });

    var pushValuesFromArray = function(viewOptions, fieldName, checkboxes) {
        checkboxes.prop("checked", false);
        if (viewOptions[fieldName]) {
            viewOptions[fieldName].forEach(function(val) {
                checkboxes.filter("[value='" + val + "']").prop("checked", true);
            });
        }
    };

    viewOptionsBt.on("click", function() {
        viewOptionsBlogs.empty().html(swig.run(viewOptionsPostsBlogTemplate, {list: postPagination.viewOptions.blogs}));
        viewOptionsTags.empty().html(swig.run(viewOptionsPostsTagTemplate, {list: postPagination.viewOptions.tags}));

        pushValuesFromArray(postPagination.viewOptions, "user_liked", viewOptionsUserLiked);
        pushValuesFromArray(postPagination.viewOptions, "types", viewOptionsTypes);

        viewOptionsOrder.val(postPagination.viewOptions.order);
        viewOptionsSkip.val(postPagination.viewOptions.skip);

        viewOptionsModal.modal({
            backdrop: "static",
            keyboard: true
        });

        return false;
    });

    var pushNumberValuesToArray = function(viewOptions, fieldName, checkboxes) {
        viewOptions[fieldName] = [];
        checkboxes.filter(":checked").each(function() {
            viewOptions[fieldName].push(Number($(this).val()));
        });
    };

    var pushValuesToArray = function(viewOptions, fieldName, checkboxes) {
        viewOptions[fieldName] = [];
        checkboxes.filter(":checked").each(function() {
            viewOptions[fieldName].push($(this).val());
        });
    };

    viewOptionsSubmitBt.on("click", function() {
        postPagination.viewOptions.blogs = [];
        viewOptionsBlogs.find("input[name=blog]").each(function() {
            postPagination.viewOptions.blogs.push($(this).val());
        });

        postPagination.viewOptions.tags = [];
        viewOptionsTags.find("input[name=tag]").each(function() {
            postPagination.viewOptions.tags.push($(this).val());
        });

        pushNumberValuesToArray(postPagination.viewOptions, "user_liked", viewOptionsUserLiked);
        pushValuesToArray(postPagination.viewOptions, "types", viewOptionsTypes);

        postPagination.viewOptions.order = Number(viewOptionsOrder.val());
        postPagination.viewOptions.skip = Number(viewOptionsSkip.val());

        viewOptionsModal.modal('hide');
        postPagination.loadPage(postPagination.viewOptions.skip);

        return false;
    });

    /*
     * End of view options block
     */

    /*
     * Support block
     */

    // Likes
    $(".posts-content").on("click", ".post .like", function() {
        var self = $(this);

        $.ajax("/posts/like", {
            dataType: "json",
            type: "GET",
            data: {
                "_id": self.data("id"),
                "like": self.hasClass("liked") ? 0 : 1
            }
        }).done(function(data) {
            if (!data.success) {
                console.dir(data.error);
                showError("Like error:", data.error);
            } else {
                if (data.payload.like) {
                    self.addClass("liked").addClass("glyphicon-heart").removeClass("glyphicon-heart-empty");
                }
                else
                {
                    self.removeClass("liked").removeClass("glyphicon-heart").addClass("glyphicon-heart-empty");
                }
            }
        }).fail(function() {
            showError("Like error:", "Server error");
        });


        return false;
    });

    //Tag form
    postsContent.on("click", ".post button.open-tag-form", function() {
        $(this).closest(".footer").find(".tagBtCell").hide();
        $(this).closest(".footer").find(".tagFormCell").show();

        return false;
    });

    postsContent.on("click", ".post button.close-tag-form", function() {
        $(this).closest(".footer").find(".tagBtCell").show();
        $(this).closest(".footer").find(".tagFormCell").hide();

        return false;
    });

    postsContent.on("click", ".post .usertag", function() {
        var self = $(this);

        $.ajax("/posts/tag", {
            type: "GET",
            dataType: "json",
            data: {
                add: 0,
                _id: self.data("id"),
                tag: self.data("tag")
            }
        }).done(function(data) {
            if (!data.success) {
                console.dir(data.error);
                showError("User tag error:", data.error);
            }
            else {
                self.remove();
            }
        }).fail(function() {
            showError("User tag error:", "Server error");
        });

        return false;
    });

    postsContent.on("submit", ".post form[name='addTagForm']", function() {
        var self = $(this);

        $.ajax("/posts/tag", {
            type: "GET",
            dataType: "json",
            data: self.serialize()
        }).done(function(data) {
            if (!data.success) {
                console.dir(data.error);
                showError("User tag error:", data.error);
            }
            else {
                /*jshint camelcase:false */
                var tags = self.closest(".post").find(".tags");

                if (tags.find("span.usertag[data-tag='" + data.payload.tag.replace("'", "\'") + "']").length === 0) {
                    self.closest(".post").find(".tags").append(swig.run(postListUserTag, {item: {_id: data.payload._id, user_tags: [data.payload.tag]}}));
                }
                /*jshint camelcase:true */
            }
        }).fail(function() {
            showError("User tag error:", "Server error");
        });

        return false;
    });


    postsContent.on('click', '*[data-toggle="lightbox"]', function() {
        $(this).ekkoLightbox();

        return false;
    });

    postsContent.on('postRender', function() {
        $(this).find(".post-content img:not(.galleryPhoto):not(.noGallery)").each(function() {
            var $this = $(this);
            var parent = $this.closest(".post-content");

            $this.attr("data-toggle", "lightbox");
            $this.attr("data-remote", $this.attr("src"));
            $this.attr("data-gallery", "post-" + parent.data("id"));
            $this.attr("data-parent", ".post-content.post-" + parent.data("id"));
            $this.attr("data-type", "image");

            $this.addClass("readyPhoto");
        });
    });

    var resizeFunc = function() {
        postPagination.loadPage(postPagination.viewOptions.skip, true);
    };

    $(window).resize(function() {
        $.debounce(resizeFunc, 150, false);
    });

    /*
     * End of support block
     */

};