/*
 * Blogs
 */

/*jshint unused:false */
var launchBlogs = function() {
    /*jshint unused:true */

    $("#blogsLinkBt, #blogsLinkBtSm").addClass("active");

    /*
     * Pagination block         
     */

    var defaultViewOptions = {
        _id: "",
        blogTitle: "",
        process: [],
        notAvailable: [],
        read: [],
        export: [],
        order: 1,
        skip: 0,
        compact: 1,
        edit: 0,
        updateImmediately: 0
    };

    var parseArray = function(array, checkValue) {
        if (!(array instanceof Array)) {
            return false;
        }

        var correctVar = true;
        array.forEach(function(v) {
            if (!checkValue(v)) {
                correctVar = false;
            }
        });
        return correctVar;
    };
    var checkYesNoValue = function(val) {
        val = Number(val);
        return (val === 0 || val === 1);
    };
    var checkProcessValue = function(val) {
        val = Number(val);
        return (val >= -1 && val <= 1);
    };

    var parseViewOptions = function(options) {
        if (!options)
        {
            return false;
        }

        for (var property in options) {
            if (!options.hasOwnProperty(property)) {
                continue;
            }

            switch (property) {
                case "_id":
                case "blogTitle":
                    options[property] = options[property].toString();
                    break;
                case "process" :
                    if (!parseArray(options[property], checkProcessValue)) {
                        continue;
                    }
                    break;
                case "export":
                case "notAvailable":
                case "read":
                    if (!parseArray(options[property], checkYesNoValue)) {
                        return false;
                    }
                    break;
                case "order":
                    options[property] = Number(options[property]);
                    if (Math.abs(options[property]) !== 1) {
                        return false;
                    }
                    break;
                case "skip":
                    options[property] = Number(options[property]);
                    if (options[property] < 0) {
                        return false;
                    }
                    break;
                case "compact":
                case "edit":
                case "updateImmediately":
                    options[property] = Number(options[property]);
                    if (options[property] !== 0 && options[property] !== 1) {
                        return false;
                    }
                    break;
                default:
                    console.log("Unknow property", property, "in blogs view options, using default");
                    return false;
            }
        }

        return true;
    };

    var saveViewOptions = function(options) {
        localStorage.setItem("blogViewSettings", JSON.stringify(options));
    };

    var wrapViewOptions = function(options) {
        /*jshint camelcase:false */
        var query = {
            _id: options._id,
            blog_title: options.blogTitle,
            order: options.order,
            skip: options.skip
        };
        if (options.process) {
            query.process = options.process;
        }
        if (options.notAvailable) {
            query.not_available = options.notAvailable;
        }
        if (options.read) {
            query.read = options.read;
        }
        if (options.export) {
            query.export = options.export;
        }

        return query;
        /*jshint camelcase:true */
    };

    var blogsContent = $(".blogs-content tbody");
    var renderContent = function(data, viewOptions) {
        data.payload.items.forEach(function(item) {
            if (item.info) {
                var date = new Date(item.info.updated * 1000);
                item.info.updated = moment(date).format("YYYY.MM.DD HH:mm:ss");
            }
        });

        return swig.run(blogsListTemplate, {items: data.payload.items, compact: viewOptions.compact, edit: viewOptions.edit});
    };

    var blogPagination = new Paginate(defaultViewOptions, parseViewOptions, saveViewOptions, wrapViewOptions, $(".prev-page"), $(".count-bt"), $(".next-page"), $("#refreshBt"), "/blogs/show", blogsContent, renderContent);
    blogPagination.initOptions(JSON.parse(localStorage.getItem("blogViewSettings")));
    blogPagination.loadPage(blogPagination.viewOptions.skip);

    /*
     * End of pagination block
     */

    /*
     * View options block
     */

    var viewOptionsBt = $("#viewOptionsBt");
    var viewOptionsModal = $("#viewOptionsModal");
    var viewOptionsForm = viewOptionsModal.find("#viewOptions");
    var viewOptionsSubmitBt = viewOptionsModal.find("#commit");

    var viewOptionsId = viewOptionsForm.find("input[name='_id']");
    var viewOptionsTitle = viewOptionsForm.find("input[name='blog_title']");

    var viewOptionsProcess = viewOptionsForm.find("input[name='process']");
    var viewOptionsNotAvailable = viewOptionsForm.find("input[name='not_available']");
    var viewOptionsRead = viewOptionsForm.find("input[name='read']");
    var viewOptionsExport = viewOptionsForm.find("input[name='export']");

    var viewOptionsOrder = viewOptionsForm.find("select[name='order']");
    var viewOptionsSkip = viewOptionsForm.find("input[name='skip']");

    var viewOptionsCompact = viewOptionsForm.find("input[name='compact']");
    var viewOptionsEdit = viewOptionsForm.find("input[name='edit']");
    var viewOptionsUpdateImmediately = viewOptionsForm.find("input[name='update_immediately']");

    var pushValuesFromArray = function(viewOptions, fieldName, checkboxes) {
        checkboxes.prop("checked", false);
        if (viewOptions[fieldName]) {
            viewOptions[fieldName].forEach(function(val) {
                checkboxes.filter("[value='" + val + "']").prop("checked", true);
            });
        }
    };

    viewOptionsId.typeahead({
        items: 'all',
        minlength: 1,
        source: function(query, cb) {
            $.ajax("/blogs/search", {
                dataType: "json",
                type: "GET",
                data: {
                    query: query
                }
            }).done(function(data) {
                if (!data.success) {
                    console.dir(data.error);
                    showError("Autocomplete error:", JSON.stringify(data.error));
                    cb([]);
                }
                else {
                    cb(data.payload);
                }
            }).fail(function() {
                showError("Autocomplete error:", "Server error");
                cb([]);
            });
        }
    });

    viewOptionsBt.on("click", function() {
        viewOptionsId.val(blogPagination.viewOptions._id);
        viewOptionsTitle.val(blogPagination.viewOptions.blogTitle);

        pushValuesFromArray(blogPagination.viewOptions, "process", viewOptionsProcess);
        pushValuesFromArray(blogPagination.viewOptions, "notAvailable", viewOptionsNotAvailable);
        pushValuesFromArray(blogPagination.viewOptions, "read", viewOptionsRead);
        pushValuesFromArray(blogPagination.viewOptions, "export", viewOptionsExport);

        viewOptionsOrder.val(blogPagination.viewOptions.order);
        viewOptionsSkip.val(blogPagination.viewOptions.skip);

        viewOptionsCompact.prop("checked", blogPagination.viewOptions.compact ? true : false);
        viewOptionsEdit.prop("checked", blogPagination.viewOptions.edit ? true : false);
        viewOptionsUpdateImmediately.prop("checked", blogPagination.viewOptions.updateImmediately ? true : false);

        viewOptionsModal.modal({
            backdrop: "static",
            keyboard: true
        });

        return false;
    });

    var pushValuesToArray = function(viewOptions, fieldName, checkboxes) {
        viewOptions[fieldName] = [];
        checkboxes.filter(":checked").each(function() {
            viewOptions[fieldName].push(Number($(this).val()));
        });
    };

    viewOptionsSubmitBt.on("click", function() {
        blogPagination.viewOptions._id = viewOptionsId.val();
        blogPagination.viewOptions.blogTitle = viewOptionsTitle.val();

        pushValuesToArray(blogPagination.viewOptions, "process", viewOptionsProcess);
        pushValuesToArray(blogPagination.viewOptions, "notAvailable", viewOptionsNotAvailable);
        pushValuesToArray(blogPagination.viewOptions, "read", viewOptionsRead);
        pushValuesToArray(blogPagination.viewOptions, "export", viewOptionsExport);

        blogPagination.viewOptions.order = Number(viewOptionsOrder.val());
        blogPagination.viewOptions.skip = Number(viewOptionsSkip.val());

        blogPagination.viewOptions.compact = viewOptionsCompact.prop("checked") ? 1 : 0;
        blogPagination.viewOptions.edit = viewOptionsEdit.prop("checked") ? 1 : 0;
        blogPagination.viewOptions.updateImmediately = viewOptionsUpdateImmediately.prop("checked") ? 1 : 0;

        viewOptionsModal.modal('hide');
        blogPagination.loadPage(blogPagination.viewOptions.skip);

        return false;
    });

    /*
     * End of view options block
     */

    /*
     * Add blog block
     */

    /**
     * 
     * @param {type} form
     * @returns {undefined}
     */
    var setFormUpdate = function(form) {
        var bounce = function() {
            if (blogPagination.viewOptions.updateImmediately) {
                form.submit();
            }
        };

        form.on("change", "input[type=radio]", $.debounce(bounce, 100, false));
        form.on("keyup", "input[type=text]", $.debounce(bounce, 100, false));
    };

    var addBlogBt = $("#addBlogBt");
    var addBlogModal = $("#addBlogModal");
    var addBlogForm = addBlogModal.find("#addBlogForm");
    var addBlogSubmit = addBlogModal.find("#commit");

    addBlogBt.on("click", function() {
        addBlogModal.modal({
            backdrop: "static",
            keyboard: true
        });

        return false;
    });

    addBlogSubmit.on("click", function() {
        $.ajax("/blogs/insert", {
            type: "POST",
            dataType: "json",
            data: addBlogForm.serialize()
        }).done(function(data) {
            if (!data.success) {
                console.dir(data.error);
                showError("Add blog error:", JSON.stringify(data.error));
            }
            else {
                addBlogModal.modal('hide');
                blogsContent.prepend(renderContent(data, blogPagination.viewOptions));
            }
        }).fail(function() {
            showError("Add blog error:", "Server error");
        });

        return false;
    });

    blogsContent.on("submit", "form.update-blog-form", function() {
        var self = $(this);

        $.ajax("/blogs/update", {
            type: "POST",
            dataType: "json",
            data: self.serialize()
        }).done(function(data) {
            if (!data.success) {
                console.dir(data.error);
                showError("Update blog error:", JSON.stringify(data.error));
            }
            else {
                var rendered = $(renderContent(data, {compact: self.closest("tr").find(".blog-info.panel.collapse").hasClass("in") ? 0 : 1, edit: 1}));

                self.closest("tr").find(".info-part").html(rendered.find(".info-part").html());
                var button = self.find(".submit-button");

                button.tooltip({
                    trigger: "manual",
                    placement: "top",
                    title: "Blog successfully updated"
                }).tooltip('show');

                button.on('shown.bs.tooltip', function() {
                    setTimeout(function() {
                        button.tooltip('destroy');
                    }, 2000);
                });
            }
        }).fail(function() {
            showError("Update blog error:", "Server error");
        });

        return false;
    });

    blogsContent.on("click", ".editBlog", function() {
        var $this = $(this);
        $this.removeClass("editBlog").addClass("viewBlog");
        $this.find(".glyphicon").removeClass("glyphicon-pencil").addClass("glyphicon-eye-open");

        $this.closest("tr").find(".left-part.info-part").hide();
        $this.closest("tr").find(".left-part.form-part").show();

        return false;
    });

    blogsContent.on("click", ".viewBlog", function() {
        var $this = $(this);
        $this.addClass("editBlog").removeClass("viewBlog");
        $this.find(".glyphicon").addClass("glyphicon-pencil").removeClass("glyphicon-eye-open");

        $this.closest("tr").find(".left-part.form-part").hide();
        $this.closest("tr").find(".left-part.info-part").show();

        return false;
    });

    /*
     * End of add blog block
     */



    /*
     * Support block 
     */

    blogsContent.on('postRender', function() {
        $(this).find("form").each(function() {
            setFormUpdate($(this));
        });
    });

    /*
     * End of support block 
     */

};