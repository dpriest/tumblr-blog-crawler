/*
 * Common functions
 */

var errorModal;
var showError = function() {
};

var successModal;
var showSuccess = function() {
};

var contentColumn;

var getWindowContentWidth = function() {
};
var windowContentWidth;

$(function() {
    getWindowContentWidth = function() {
        var windowWidth = $(window).width();

        if (windowWidth >= 1200) {
            return 603;
        }

        if (windowWidth >= 992) {
            return 565;
        }

        if (windowWidth >= 768) {
            return 538;
        }

        return windowWidth - 92;
    };
    windowContentWidth = getWindowContentWidth();

    contentColumn = $("#content-column");

    (function() { // Popups block
        errorModal = $("#errorModal");
        showError = function() {
            var errors = [];
            for (var i = 0; i < arguments.length; i++) {
                errors.push(arguments[i].toString());
            }
            var error = errors.join(" ");
            console.log(error);
            errorModal.find(".modal-body").html(error);
            errorModal.modal({
                backdrop: 'static',
                keyboard: true
            });
        };
        successModal = $("#successModal");
        showSuccess = function() {
            var msgs = [];
            for (var i = 0; i < arguments.length; i++) {
                msgs.push(arguments[i].toString());
            }
            var msg = msgs.join(" ");
            successModal.find(".modal-body").html(msg);
            successModal.modal({
                backdrop: 'static',
                keyboard: true
            });
        };
    })(); // End of popups blogs



    (function() { // Auth block
        var authModal = $("#authorizeModal");
        var authForm = authModal.find("#authForm");
        var authCommitBt = authModal.find("#commit");
        var authAction = authForm.find("input#action");

        $("#authBtn, #authBtnSm").on('click', function() { // Show auth modal button
            $.ajax("/sessions/check", {
                dataType: "json",
                type: "POST"
            }).done(function(data) {
                if (!data.success) {
                    console.dir(data.error);
                    showError("Auth check error:", JSON.stringify(data.error));
                } else {
                    authForm.toggle(data.payload.auth === false);
                    authCommitBt.html(data.payload.auth === false ? "Log in" : "Log out");
                    authAction.val(data.payload.auth === false ? "login" : "logon");
                    authModal.modal({
                        backdrop: 'static',
                        keyboard: true
                    });
                }
            }).fail(function() {
                showError("Auth check error:", "Server error");
            });

            return false;
        });

        authCommitBt.on("click", function() { // Auth button
            var link, params;
            if (authAction.val() === "login") {
                link = "/sessions/login";
                params = authForm.serialize();
            } else {
                link = "/sessions/logout";
                params = {};
            }

            $.ajax(link, {
                data: params,
                dataType: "json",
                type: "POST"
            }).done(function(data) {
                if (!data.success) {
                    console.dir(data.error);
                    showError("Auth error:", JSON.stringify(data.error));
                } else {
                    successModal.on('hidden.bs.modal', function() {
                        successModal.off('hidden.bs.modal');
                        authModal.modal('hide');
                    });
                    showSuccess("Auth successful");
                }
            }).fail(function() {
                showError("Auth error:", "Server error");
            });

            return false;
        });
    })(); // End of auth block



    (function() { // Get data block
        var getBlogsByTagsBt = $("#getBlogsByTags, #getBlogsByTagsSm");
        var getBlogsByLinksBt = $("#getBlogsByLinks, #getBlogsByLinksSm");
        var getBlogsByPostsBt = $("#getBlogsByPosts, #getBlogsByPostsSm");
        var getBlogsInfoBt = $("#getBlogsInfo, #getBlogsInfoSm");
        var getPostsBt = $("#getPosts, #getPostsSm");
        var getUpdatePostsBt = $("#getUpdatePosts, #getUpdatePostsSm");
        var importFollowersBt = $("#importFollowers, #importFollowersSm");
        var importFollowingsBt = $("#importFollowings, #importFollowingsSm");

        var process = function(btn, link, taskName) {
            btn.on('click', function() {
                $.ajax(link, {
                    dataType: "json",
                    type: "GET"
                }).done(function(data) {
                    if (!data.success) {
                        console.dir(data.error);
                        showError(taskName + " error:", JSON.stringify(data.error));
                    } else {
                        showSuccess("Task added", taskName);
                    }
                }).fail(function() {
                    showError(taskName + " error:", "Server error");
                });

                return false;
            });
        };

        process(getBlogsByTagsBt, "/tags/process", "Get blogs by tags");
        process(getBlogsByLinksBt, "/blogs/process", "Get blogs by links");
        process(getBlogsByPostsBt, "/blogs/process-posts", "Get blogs by posts");
        process(getBlogsInfoBt, "/blogs/get-info", "Update blogs info");
        process(getPostsBt, "/posts/process", "Read blogs");
        process(getUpdatePostsBt, "/posts/update", "Update all posts");
        process(importFollowersBt, "/blogs/followers", "Import followers");
        process(importFollowingsBt, "/blogs/followings", "Import followees");
    })(); // End of get data block



    (function() { // Get health block
        var getHealthBt = $("#getHealth, #getHealthSm");
        var healthModal = $("#healthModal");
        var healthContent = healthModal.find("table.health-content tbody");

        getHealthBt.on("click", function() {
            $.ajax("/utils/health", {
                dataType: "json",
                type: "GET"
            }).done(function(data) {
                if (!data.success) {
                    console.dir(data.error);
                    showError("Status error:", JSON.stringify(data.error));
                } else {
                    healthContent.empty();
                    $.each(data.payload, function(categoryName, category) {
                        healthContent.append('<tr><td colspan="2"><h4><strong>' + categoryName + '</strong></h4></td></tr>');

                        $.each(category, function(key, value) {
                            healthContent.append('<tr><td><strong>' + key + '</strong></td><td>' + value + '</td></tr>');
                        });
                    });

                    healthModal.modal({
                        backdrop: 'static',
                        keyboard: true
                    });
                }
            }).fail(function() {
                showError("Status error:", "Server error");
            });

            return false;
        });
    })(); // End of get health block



    (function() { // Import by parsing blog
        var importBt = $("#importParsing, #importParsingSm");
        var importModal = $("#importParsingModal");
        var importForm = importModal.find("#importParsingForm");
        var importSubmitBt = importModal.find("#commit");

        importBt.on("click", function() {
            importModal.modal({
                backdrop: 'static',
                keyboard: true
            });
        });

        importSubmitBt.on("click", function() {
            $.ajax("/blogs/import", {
                data: importForm.serialize(),
                dataType: "json",
                type: "POST"
            }).done(function(data) {
                if (!data.success) {
                    showError("Import error:", data.error);
                }
                else
                {
                    successModal.on('hidden.bs.modal', function() {
                        successModal.off('hidden.bs.modal');
                        importModal.modal('hide');
                    });
                    showSuccess("Import task added");
                }
            }).fail(function() {
                showError("Import error:", "Server error");
            });
        });
    })(); // End of import by parsing blog



    (function() { // User import block
        var authBt = $("#authOAuthBtn, #authOAuthBtnSm");
        authBt.on("click", function() {
            $.ajax("/sessions/oauth-authorize", {
                type: "GET",
                dataType: "json"
            }).done(function(data) {
                if (!data.success) {
                    console.dir(data.error);
                    showError("OAuth error:", data.error);
                } else {
                    window.location = "http://www.tumblr.com/oauth/authorize?oauth_token=" + data.payload.token;
                    console.dir(data.payload);
                }
            }).fail(function() {
                showError("OAuth error:", "Server error");
            });
        });
    })(); // End of user import block
});

/*
 * Pagination object
 */
var Paginate = function(defaultViewOptions, parseViewOptions, saveViewOptions, viewOptionsWrapper, prevPageBt, countBt, nextPageBt, refreshBt, contentUri, contentContainer, renderContent) {
    this.pageLoading = false;
    this.viewOptions = {};

    this.defaultViewOptions = defaultViewOptions;
    this.parseViewOptions = parseViewOptions;
    this.saveViewOptions = saveViewOptions;
    this.viewOptionsWrapper = viewOptionsWrapper;

    this.prevPageBt = prevPageBt;
    this.countBt = countBt;
    this.nextPageBt = nextPageBt;
    this.refreshBt = refreshBt;

    this.contentUri = contentUri;
    this.contentContainer = contentContainer;
    this.renderContent = renderContent;

    var self = this;
    (function() { //Init buttons
        self.prevPageBt.on("click", function() {
            self.loadPage(self.viewOptions.skip - 1);
            $(this).blur();
            return false;
        });
        self.nextPageBt.on("click", function() {
            self.loadPage(self.viewOptions.skip + 1);
            $(this).blur();
            return false;
        });
        self.refreshBt.on("click", function() {
            self.loadPage(self.viewOptions.skip, true);
            $(this).blur();
            return false;
        });
        self.countBt.on("click", function() {
            if (contentColumn.offset().top < $(window).scrollTop()) {
                $.scrollTo(contentColumn.offset().top, 300);
            }
            return false;
        });
    })();

    return this;
};

/**
 * 
 *
 * @param {Object} viewOptions
 * @returns {undefined}
 */
Paginate.prototype.initOptions = function(viewOptions) {
    this.viewOptions = viewOptions;
    if (!this.parseViewOptions(this.viewOptions)) {
        this.viewOptions = this.defaultViewOptions;
    }
};

/**
 * Updating buttons
 *
 * @param {type} hasNext
 * @param {Number} count
 * @returns {undefined}
 */
Paginate.prototype.refreshButtons = function(hasNext, count) {
    $(this.prevPageBt).toggle(this.viewOptions.skip > 0);
    $(this.nextPageBt).toggle(hasNext === true);
    $(this.countBt).show().find("span.value").html(count);
};

/**
 * Load page
 *
 * @param {type} page
 * @returns {undefined}
 */
Paginate.prototype.loadPage = function(page, dontScroll) {
    if (this.pageLoading) {
        showError("Page is loading, wait a bit");
        return;
    }

    this.pageLoading = true;
    this.viewOptions.skip = Math.max(page, 0);
    this.saveViewOptions(this.viewOptions);

    $.ajax(this.contentUri, {
        type: "POST",
        dataType: "json",
        data: this.viewOptionsWrapper(this.viewOptions)
    }).done((function(data) {
        if (!data.success) {
            console.dir(data.error);
            showError("Page loading error:", JSON.stringify(data.error));
        } else {
            if (!dontScroll) {
                if (contentColumn.offset().top < $(window).scrollTop()) {
                    $.scrollTo(contentColumn.offset().top, 300);
                }
            }
            windowContentWidth = getWindowContentWidth();

            this.refreshButtons(data.payload.hasNext, data.payload.count);
            this.contentContainer.empty().html(this.renderContent(data, this.viewOptions)).trigger("postRender");
        }
    }).bind(this)).fail(function() {
        showError("Page loading error:", "Server error");
    }).always((function() {
        this.pageLoading = false;
    }).bind(this));
};