/**
 * Posts Dao object
 *
 * @returns {PostsDao}
 */
var PostsDao = function()
{
    this.parent.apply(this, arguments);

    this.collection = "posts";

    return this;
};

PostsDao.prototype = require("./basicDAO")();
PostsDao.prototype.parent = PostsDao.prototype.constructor;
PostsDao.prototype.constructor = PostsDao;

module.exports = function() {
    var o = Object.create(PostsDao.prototype);
    return PostsDao.apply(o, arguments);
};
