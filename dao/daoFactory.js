/**
 * Dao factory
 * Instead of creating instances of dao directly, you should use this factory
 *
 * @param {type} db
 * @param {Object} config
 * @returns {DaoFactory}
 */
var DaoFactory = function(db, config) {
    this.db = db;
    this.config = config;

    this.tagsDao = null;
    this.blogsDao = null;
    this.postsDao = null;
    this.sessionsDao = null;

    return this;
};

/**
 * Autoloader
 *
 * @returns {undefined}
 */
DaoFactory.prototype.__preload = function() {
    console.log('Preloading dao factory');

    this.getTagsDao();
    this.getBlogsDao();
    this.getPostsDao();
    this.getSessionsDao();
};

/**
 * Getting tags Dao object
 *
 * @returns {TagsDao}
 */
DaoFactory.prototype.getTagsDao = function() {
    this.tagsDao = this.tagsDao || require("./tagsDAO")(this.db, this.config);
    return this.tagsDao;
};

/**
 * Getting blogs Dao object
 *
 * @returns {BlogsDao}
 */
DaoFactory.prototype.getBlogsDao = function() {
    this.blogsDao = this.blogsDao || require("./blogsDAO")(this.db, this.config);
    return this.blogsDao;
};

/**
 * Getting posts Dao object
 *
 * @returns {PostsDao}
 */
DaoFactory.prototype.getPostsDao = function() {
    this.postsDao = this.postsDao || require("./postsDAO")(this.db, this.config);
    return this.postsDao;
};

/**
 * Getting sessions Dao object
 *
 * @returns {SessionsDao}
 */
DaoFactory.prototype.getSessionsDao = function() {
    this.sessionsDao = this.sessionsDao || require("./sessionsDAO")(this.db, this.config);
    return this.sessionsDao;
};

module.exports = function() {
    var o = Object.create(DaoFactory.prototype);
    return DaoFactory.apply(o, arguments);
};