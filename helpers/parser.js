var request = require("request");

/**
 * Various common function
 *
 * @param {Object} config
 * @returns {Parser}
 */
var Parser = function(config) {
    this.config = config;

    return this;
};

Parser.prototype.parseRegexp = /href\s*=\s*["']?([^"'<>\s]*)(["'<>\s]|$)/ig;

/**
 * Connecting to page via request
 *
 * @param {String} link
 * @param {Function} callback
 * @returns {undefined}
 */
Parser.prototype.connect = function(link, callback) {
    var self = this;

    request(
            {
                method: 'GET',
                uri: link,
                timeout: self.config.request.timeout,
                followRedirect: self.config.request.followRedirect,
                maxRedirects: self.config.request.maxRedirects
            },
    function(err, response, body) {
        if (err)
            return callback(err, null);

        return callback(err, body);
    });
};

/**
 * Prepare link for collection
 *
 * @param {String} link
 * @returns {String|Boolean}
 */
Parser.prototype.prepareLink = function(link)
{
    link = link.toString().toLowerCase().replace(/^http:\/\//i, "").replace(/^https:\/\//i, "").replace(/^www\./, "").replace(/\/.*$/, "").replace(/\s/, "");

    if (!link.match(/^mailto:/) && !link.match(/^#/) && !link.match(/^javascript:/) && link && link.match(/^[a-z0-9_-]+(\.[a-z0-9_-]+)+/i))
    {
        return link;
    }

    return false;
};

/**
 * Parse text for links
 *
 * @param {String} body
 * @returns {Array}
 */
Parser.prototype.parse = function(body) {
    body = body.toString();
    var match = this.parseRegexp.exec(body);
    var result = [];
    var link;

    while (match != null) {
        link = this.prepareLink(match[1]);
        if (link !== false)
        {
            result.push(link);
        }

        match = this.parseRegexp.exec(body);
    }
    return result;
};

var parser = null;
module.exports = function(config) {
    parser = parser || new Parser(config);
    return parser;
};
