/*
 * Autoloader module
 */
var walk = require("walkdir");

var walkCb = function(path, stat) {
    if (stat.isFile()) {
        console.log("Loading module", path);
        require(path);
    }
};

var _preload = function(object) {
    if (!(object instanceof Object)) {
        return;
    }
    if (object.hasOwnProperty("__preloaded") && object.__preloaded) {
        return;
    }

    if (object.__preload instanceof Function) {
        object.__preloaded = true;
        object.__preload();

        for (var field in object) {
            _preload(object[field]);
        }
    }
};


module.exports = {
    bootstrap: function(map) {
        if (map instanceof Array) {
            for (var i = 0; i < map.length; i++)
            {
                walk.sync(map[i], walkCb);
            }
        }
    },
    preload: _preload
};