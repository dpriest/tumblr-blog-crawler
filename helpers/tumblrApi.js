var request = require("request");
var querystring = require("querystring");
var OAuth = require("oauth");

/**
 * Tumblr Api bicycle object
 *
 * @param {Object} config
 * @returns {TumblrApi}
 */
var TumblrApi = function(config)
{
    this.config = config;
    this.oauth = new OAuth.OAuth(
            'https://www.tumblr.com/oauth/request_token',
            'https://www.tumblr.com/oauth/access_token',
            this.config.tumblr_api_auth.consumer_key,
            this.config.tumblr_api_auth.consumer_secret,
            '1.0A',
            this.config.tumblr_api_auth.callback,
            'HMAC-SHA1');

    return this;
};

/**
 * Uri for blog info
 *
 * @param {String} blog
 * @returns {String}
 */
TumblrApi.prototype.getBlogInfoUri = function(blog) {
    return 'https://api.tumblr.com/v2/blog/' + blog + '/info?' + querystring.stringify({api_key: this.config.tumblr_api_auth.consumer_key});
};

/**
 * Uri for tags
 *
 * @param {String} tag
 * @param {Number} before
 * @returns {String}
 */
TumblrApi.prototype.getTaggedUri = function(tag, before) {
    var params = {
        api_key: this.config.tumblr_api_auth.consumer_key,
        tag: tag,
        limit: this.config.tagged_pages.limit
    };
    if (before !== 0) {
        params.before = before;
    }
    return 'https://api.tumblr.com/v2/tagged?' + querystring.stringify(params);
};

/**
 * Uri for posts
 *
 * @param {String} blog
 * @param {Number} offset
 * @returns {String}
 */
TumblrApi.prototype.getPostsUri = function(blog, offset) {
    var params = {
        api_key: this.config.tumblr_api_auth.consumer_key,
        offset: offset,
        limit: this.config.read_pages.limit
    };
    return 'https://api.tumblr.com/v2/blog/' + blog + '/posts?' + querystring.stringify(params);
};

/**
 * Uri for post by id
 *
 * @param {String} blog
 * @param {Number} id
 * @returns {String}
 */
TumblrApi.prototype.getPostByIdUri = function(blog, id) {
    var params = {
        api_key: this.config.tumblr_api_auth.consumer_key,
        id: id
    };
    return 'https://api.tumblr.com/v2/blog/' + blog + '/posts?' + querystring.stringify(params);
};

/**
 * Uri for user info
 *
 * @returns {String}
 */
TumblrApi.prototype.getUserInfoUri = function() {
    return 'https://api.tumblr.com/v2/user/info/';
};

/**
 * Uri for followers
 *
 * @param {String} blog
 * @param {Number} offset
 * @returns {String}
 */
TumblrApi.prototype.getFollowersUri = function(blog, offset) {
    var params = {
        offset: offset,
        limit: this.config.followers_pages.limit
    };
    return 'https://api.tumblr.com/v2/blog/' + blog + '/followers?' + querystring.stringify(params);
};

/**
 * Uri for followees
 *
 * @param {Number} offset
 * @returns {String}
 */
TumblrApi.prototype.getFollowingsUri = function(offset) {
    var params = {
        offset: offset,
        limit: this.config.followers_pages.limit
    };
    return 'https://api.tumblr.com/v2/user/following?' + querystring.stringify(params);
};

/**
 * Getting blog info
 *
 * @param {String} blog
 * @param {Function} callback
 * @returns {undefined}
 */
TumblrApi.prototype.getBlogInfo = function(blog, callback) {
    console.log("Requesting info for ", blog);
    request({
        method: 'GET',
        uri: this.getBlogInfoUri(blog),
        timeout: this.config.request.timeout,
        followRedirect: this.config.request.followRedirect,
        maxRedirects: this.config.request.maxRedirects
    }, function(err, response, body) {
        if (err)
        {
            return callback(err);
        }
        try {
            var data = JSON.parse(body);
            if (data.meta.status !== 200) {
                console.log("Tumblr Api error: ", data.meta.msg);
                return callback(null, false);
            }

            callback(null, data.response.blog);
        }
        catch (ex) {
            callback(ex);
        }
    });
};

/**
 * Getting posts with tag
 *
 * @param {String} tag
 * @param {Number} before
 * @param {Function} callback
 * @returns {undefined}
 */
TumblrApi.prototype.getTaggedPosts = function(tag, before, callback) {
    console.log("Looking for tagged posts, tag ", tag, " timestamp ", before);
    request({
        method: 'GET',
        uri: this.getTaggedUri(tag, before),
        timeout: this.config.request.timeout,
        followRedirect: this.config.request.followRedirect,
        maxRedirects: this.config.request.maxRedirects
    }, function(err, response, body) {
        if (err)
        {
            return callback(err);
        }
        try {
            var data = JSON.parse(body);
            if (data.meta.status !== 200) {
                console.log("Tumblr Api error: ", data.meta.msg);
                return callback(null, false);
            }

            callback(null, data.response);
        }
        catch (ex) {
            callback(ex);
        }
    });
};

/**
 * Getting posts from blog
 *
 * @param {String} blog
 * @param {Number} offset
 * @param {Function} callback
 * @returns {undefined}
 */
TumblrApi.prototype.getPosts = function(blog, offset, callback) {
    console.log("Looking for blog ", blog, " offset ", offset);
    request({
        method: 'GET',
        uri: this.getPostsUri(blog, offset),
        timeout: this.config.request.timeout,
        followRedirect: this.config.request.followRedirect,
        maxRedirects: this.config.request.maxRedirects
    }, function(err, response, body) {
        if (err)
        {
            return callback(err);
        }
        try {
            var data = JSON.parse(body);
            if (data.meta.status !== 200) {
                console.log("Tumblr Api error: ", data.meta.msg);
                return callback(null, false);
            }

            callback(null, data.response.posts);
        }
        catch (ex) {
            callback(ex);
        }
    });
};

/**
 * Requests token for authorization
 *
 * @param {Function} callback
 * @returns {undefined}
 */
TumblrApi.prototype.oauthAuthorization = function(callback) {
    this.oauth.getOAuthRequestToken(function(err, oauth_token, oauth_token_secret) {
        if (err) {
            callback(err);
        }
        else {
            callback(null, oauth_token, oauth_token_secret);
        }
    });
};

/**
 * Requests access token for authorization
 *
 * @param {Object} input
 * @param {Function} callback
 * @returns {undefined}
 */
TumblrApi.prototype.oauthAccess = function(auth_token, auth_secret, auth_verifier, callback) {
    this.oauth.getOAuthAccessToken(auth_token, auth_secret, auth_verifier, function(err, access_token, access_secret) {
        if (err) {
            callback(err);
        }
        else {
            callback(null, access_token, access_secret);
        }
    });
};

/**
 * Getting user info
 *
 * @param {String} blog
 * @param {Number} offset
 * @param {Function} callback
 * @returns {undefined}
 */
TumblrApi.prototype.getUserInfo = function(access_token, access_secret, callback) {
    console.log("Looking for user info ", access_token);

    this.oauth.getProtectedResource(this.getUserInfoUri(), "GET", access_token, access_secret, function(err, data) {
        if (err)
        {
            return callback(err);
        }
        try {
            var parsedData = JSON.parse(data);
            if (parsedData.meta.status !== 200) {
                console.log("Tumblr Api error: ", data.meta.msg);
                return callback(parsedData.meta.msg);
            }

            callback(null, parsedData.response.user);
        }
        catch (ex) {
            callback(ex);
        }
    });
};

/**
 * Getting followers
 *
 * @param {String} blog
 * @param {String} access_token
 * @param {String} access_secret
 * @param {Number} offset
 * @param {Function} callback
 * @returns {undefined}
 */
TumblrApi.prototype.getFollowers = function(blog, access_token, access_secret, offset, callback) {
    console.log("Looking for followers of blog ", blog, " offset ", offset);

    this.oauth.getProtectedResource(this.getFollowersUri(blog, offset), "GET", access_token, access_secret, function(err, data) {
        if (err)
        {
            return callback(err);
        }
        try {
            var parsedData = JSON.parse(data);
            if (parsedData.meta.status !== 200) {
                console.log("Tumblr Api error: ", data.meta.msg);
                return callback(parsedData.meta.msg);
            }

            callback(null, parsedData.response.users);
        }
        catch (ex) {
            callback(ex);
        }
    });
};

/**
 * Getting followees
 *
 * @param {String} access_token
 * @param {String} access_secret
 * @param {Number} offset
 * @param {Function} callback
 * @returns {undefined}
 */
TumblrApi.prototype.getFollowings = function(access_token, access_secret, offset, callback) {
    console.log("Looking for followers of user ", access_token, " offset ", offset);

    this.oauth.getProtectedResource(this.getFollowingsUri(offset), "GET", access_token, access_secret, function(err, data) {
        if (err)
        {
            return callback(err);
        }
        try {
            var parsedData = JSON.parse(data);
            if (parsedData.meta.status !== 200) {
                console.log("Tumblr Api error: ", data.meta.msg);
                return callback(parsedData.meta.msg);
            }

            callback(null, parsedData.response.blogs);
        }
        catch (ex) {
            callback(ex);
        }
    });
};

/**
 * Getting post from blog by id
 *
 * @param {String} blog
 * @param {Number} id
 * @param {Function} callback
 * @returns {undefined}
 */
TumblrApi.prototype.getPostById = function(blog, id, callback) {
    console.log("Looking for post ", id, " from blog ", blog);
    request({
        method: 'GET',
        uri: this.getPostByIdUri(blog, id),
        timeout: this.config.request.timeout,
        followRedirect: this.config.request.followRedirect,
        maxRedirects: this.config.request.maxRedirects
    }, function(err, response, body) {
        if (err)
        {
            return callback(err);
        }
        try {
            var data = JSON.parse(body);
            if (data.meta.status !== 200) {
                console.log("Tumblr Api error: ", data.meta.msg);
                return callback(null, false);
            }

            callback(null, data.response.posts);
        }
        catch (ex) {
            callback(ex);
        }
    });
};


var tumblrApi = null;
module.exports = function(config) {
    tumblrApi = tumblrApi || new TumblrApi(config);
    return tumblrApi;
};