var config = require("./config"); //Config file
var loader = require("./helpers/bootstrap"); //Let's bootstrap modules into memory

if (config.loader.getter.bootstrap) {
    loader.bootstrap(config.loader.getter.map);
}

var MongoClient = require('mongodb').MongoClient; // Mongo DB client

var taskManager = require("./helpers/taskManager"); //Task manager
var daoFactory = require("./dao/daoFactory");
var blogsModel = require("./models/blogs");
var postsModel = require("./models/posts");
var tagsModel = require("./models/tags");
var commander = require("commander");

MongoClient.connect(config.mongodb_connection_line, function(err, db) {
    "use strict";
    if (err) {
        throw err;
    }

    var manager = taskManager(config.taskManager, false);

    manager.on("start", function() {
        console.log("Starting work");
    }).on("finish", function() {
        console.log("Finished work");
    }).on("drain", function() {
        console.log("Drained!");
        process.exit();
    });

    var factory = daoFactory(db, config);
    var blogs = null;
    var posts = null;
    var tags = null;

    var needToLaunch = false;

    commander.version("0.3.8")
            .option("-l, --links", "Every blog marked as processed will be visited to get links on unvisited blogs")
            .option("-i, --info", "Blogs info will be updated")
            .option("-t, --tags", "Every tagged pages will be visited to get links on unvisited blogs")
            .option("-p, --posts", "Unread posts will be grabbed from marked blogs")
            .option("-P, --parseposts", "Visit all posts from base and parse them for links")
            .option("-u, --updateposts", "Revisit all parsed posts")
            .parse(process.argv);

    if (commander.links) {
        needToLaunch = true;

        blogs = blogs || blogsModel(factory, manager, config);
        if (config.loader.getter.preload) {
            loader.preload(blogs);
        }

        manager.addTask({}, function(data, finishTask) {
            blogs.process(function(err) {
                if (err) {
                    console.dir(err);
                    process.exit();
                }
                finishTask();
            });
        });
    }

    if (commander.info) {
        needToLaunch = true;

        blogs = blogs || blogsModel(factory, manager, config);
        if (config.loader.getter.preload) {
            loader.preload(blogs);
        }

        manager.addTask({}, function(data, finishTask) {
            blogs.getInfo(function(err) {
                if (err) {
                    console.dir(err);
                    process.exit();
                }
                finishTask();
            });
        });
    }

    if (commander.tags) {
        needToLaunch = true;

        tags = tags || tagsModel(factory, manager, config);
        if (config.loader.getter.preload) {
            loader.preload(tags);
        }

        manager.addTask({}, function(data, finishTask) {
            tags.process(function(err) {
                if (err) {
                    console.dir(err);
                    process.exit();
                }
                finishTask();
            });
        });
    }

    if (commander.posts) {
        needToLaunch = true;

        posts = posts || postsModel(factory, manager, config);
        if (config.loader.getter.preload) {
            loader.preload(posts);
        }

        manager.addTask({}, function(data, finishTask) {
            posts.startRead(function(err) {
                if (err) {
                    console.dir(err);
                    process.exit();
                }
                finishTask();
            });
        });
    }

    if (commander.parseposts) {
        needToLaunch = true;

        blogs = blogs || blogsModel(factory, manager, config);
        if (config.loader.getter.preload) {
            loader.preload(blogs);
        }

        manager.addTask({}, function(data, finishTask) {
            blogs.processPosts(function(err) {
                if (err) {
                    console.dir(err);
                    process.exit();
                }
                finishTask();
            });
        });
    }

    if (commander.updateposts) {
        needToLaunch = true;

        posts = posts || postsModel(factory, manager, config);
        if (config.loader.getter.preload) {
            loader.preload(posts);
        }

        manager.addTask({}, function(data, finishTask) {
            posts.startUpdatePosts(function(err) {
                if (err) {
                    console.dir(err);
                    process.exit();
                }
                finishTask();
            });
        });
    }

    if (!needToLaunch)
    {
        console.log("Print -h or --help key to get options");
        process.exit();
    }

    manager.start();
}); 